<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Nepal Mason E-commercial Site">
    <meta name="keywords" content="Clothes, Dress, Uniform">
    <meta name="author" content="Arash Rostami">
    <meta name="theme-color" content="#4285f4">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Error Page of NepalMason</title>

    <link href="{{ mix('css/app.css' )}}" rel="stylesheet">

</head>
<body>

<div id="app">

                @component('alert')
                    بازگشت به صفحه اصلی
                @endcomponent


</div>
<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>


</body>


</html>


