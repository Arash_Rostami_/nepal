<div class="col-md-8 main mx-auto d-block">
    <div class="card ">
        <div class="card-header text-center">
            <h3>مشاهده برند ها</h3>
        </div>
        <div class="card-body text-center">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>نام برند</th>
                    <th>کشور برند</th>
                    <th>تصویر برند</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($brands as $brand)
                    <tr>
                        <td class="table">{{$brand->name}}</td>
                        <td class="table">{{$brand->country}}</td>
                        <td class="table">
                            <img src="{{$brand->imagepath}}" width="120%">
                        </td>
                        <td class="table">
                            <input type="submit" class="btn btn-danger"
                                   value="حذف" @click="deletebrand({{$brand->id}})">
                            @csrf()
                        </td>
                    </tr>
                @endforeach

                </tbody>

            </table>

        </div>
        <div class="card-footer text-center ">
            <h6>{{ $brands->render() }}</h6>
        </div>
    </div>
</div>