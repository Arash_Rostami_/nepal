<div class="col-md-10 main mx-auto d-block">
    <div class="card ">
        <div class="card-header text-center">
            <h3>مشاهده سفارشات</h3>
        </div>
        <div class="card-body text-center">
            <table class="table table-bordered table-hover table-responsive p-2">
                <thead class="text-center p-2 bg-info">
                <tr>
                    <th>شماره سفارش</th>
                    <th>نام</th>
                    <th>ایمیل</th>
                    <th>شماره فاکتور</th>
                    <th>شماره تراکنش</th>
                    <th>نوع پرداخت</th>
                    <th> وضعیت پرداخت</th>
                    <th> وضعیت پیگیری</th>
                    <th> تاریخ خرید</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td class="table" style="width:10%">{{$order->id}}</td>
                        <td class="table" style="width:10%">
                            {{$order->user->name}}
                        </td>
                        <td class="table" style="width:10%">
                            {{$order->user->email}}</td>
                        <td class="table" style="width:10%">{{$order->id_orders}}</td>
                        <td class="table" style="width:10%">{{$order->id_trans}}</td>
                        @if($order->type =='real')
                            <td class="table" style="width:10%">نقدی در محل</td>
                        @else
                            <td class="table" style="width:10%">آنلاین</td>
                        @endif
                        @if($order->status ==0)
                            <td class="table text-center" style="width:10%"><i
                                        class="far fa-times-circle text-danger"></i></td>
                        @else
                            <td class="table text-center" style="width:10%"><i
                                        class="fas fa-check-circle text-success"></i></td>
                        @endif
                        <td class="table text-center" style="width:10%" onclick="changeAction({{$order->id}})">
                            @if($order->action == 0)
                                <span class="text-dark" style="cursor: pointer">
                                    <i class="fas fa-sync "></i> بررسی
                                </span>
                            @elseif($order->action ==1)
                                <span class="text-success" style="cursor: pointer">
                                    <i class="fa fa-truck" style="font-size: larger"></i>ارسال شده
                                </span>
                            @endif
                        </td>
                        <td class="table" style="width:10%">{{$order->date}}</td>
                        <td class="table text-center" style="width:10%">
                            <button type="button" class="btn btn-info btn-sm"
                                    data-toggle="modal" data-target="#myOrder{{$order->id}}">جزییات
                            </button>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        <div class="card-footer text-center ">
            <h6>{{$orders->render()}}</h6>
        </div>
    </div>
</div>

@include('partials.orderdetails')

<br>
