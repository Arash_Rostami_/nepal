<div class="modal fade" id="myAddressModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-primary left">
                <h4 class="text-light">افزودن آدرس</h4>
                <button type="button" class="close-pro-modal" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="/address" method="post">
                    @csrf
                    <input type="hidden" name="edit" :value="edit">
                    <input type="hidden" name="id" :value="address.id">
                    <input type="hidden" name="user" value="{{Auth::id()}}">


                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="d-inline">نام و نام خانوادگی تحویل گیرنده</label>
                            </div>
                            <div class="col-sm-8">
                                <input class="d-inline form-control" type="text" name="name" placeholder=""
                                       aria-describedby="helpId" :value="address.name" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="d-inline">شماره تماس موبایل گیرنده</label>
                            </div>
                            <div class="col-sm-8">
                                <input class="d-inline form-control" type="text" name="mobile" placeholder=""
                                       aria-describedby="helpId" :value="address.mobile" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="d-inline"> استان/شهر گیرنده </label>
                            </div>
                            @include('partials.city')
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="d-inline">آدرس پستی گیرنده</label>
                            </div>
                            <div class="col-sm-8">
                                <textarea class="d-inline form-control" type="text" name="address" placeholder=""
                                          aria-describedby="helpId" rows="4" :value="address.address"
                                          required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="d-inline">کدپستی گیرنده</label>
                            </div>
                            <div class="col-sm-8">
                                <input class="d-inline form-control" type="text" name="zipcode" placeholder=""
                                       aria-describedby="helpId" :value="address.zipcode" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="d-inline">شماره تماس دیگر (اختیاری)</label>
                            </div>
                            <div class="col-sm-8">
                                <input class="d-inline form-control" type="text" name="phone" placeholder=""
                                       aria-describedby="helpId" :value="address.phone">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn-danger form-control" data-dismiss="modal">انصراف
                                </button>
                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn-primary form-control">ثبت اطلاعات</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">


            </div>
        </div>

    </div>
</div>