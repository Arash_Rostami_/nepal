@extends('admin.profile')

@section('profile')

    <div class="col-sm-8">
        <div class="row my-3 ml-5">
            <div class="col-md-12 cat-menu text-right h-auto">
                <div id="accordionTwo">
                    <div class="card ">
                        <div class="card-header profile-card-menu">
                            <a class="card-link text-light" data-toggle="collapse" href="#collapseTwo">
                                آدرس
                            </a>
                        </div>
                        <div id="collapseTwo" class="collapse show" data-parent="#accordionTwo">
                            <div class="card-body bg-light">
                                <div class="row">
                                    <div class=" col-sm-6">

                                        <h5 class="p-2 mr-5 pr-5">آدرس محل تحویل سفارش خودرا انتخاب و یا آدرس جدید
                                            اضافه کنید</h5>

                                    </div>
                                    <div class=" col-sm-5">

                                        <a hef="#"
                                           class="btn btn-default btn-shopping-address align-middle d-flex mx-auto text-dark"
                                           data-toggle="modal" data-target="#myAddressModal">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp;&nbsp;افزودن
                                            آدرس جدید</a>
                                    </div>
                                    <div class=" col-sm-1"></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-1"></div>
                                    <div class=" col-sm-10">
                                        @foreach($addresses as $address)
                                            @if(checkAddress($address))
                                                <table class="table border user-address">
                                                    <tbody>
                                                    <tr>
                                                        <td rowspan="2" class="table-address-radio text-center">
                                                            <input type="radio" class="mt-4" onclick="enableUser()"
                                                                   value="{{$address->id ?? ''}}">
                                                        </td>
                                                        <td>
                                                            <span class="text-dark font-weight-bolder">نام تحویل گیرنده:  &nbsp;</span>
                                                            <span class="text-secondary">  {{$address->name ?? ''}}</span>

                                                        </td>
                                                        <td>
                                                            <span class="text-dark font-weight-bolder">شماره تماس ثابت:  &nbsp;</span>
                                                            <span class="text-secondary"> {{$address->phone ?? ''}} </span>
                                                        </td>
                                                        <td>
                                                            <span class="text-dark font-weight-bolder">شماره تماس موبایل:  &nbsp;</span>
                                                            <span class="text-secondary"> {{$address->mobile ?? ''}} </span>
                                                        </td>
                                                        <td class="table-address-edit text-center"
                                                            data-toggle="modal" data-target="#myAddressModal"
                                                            @click="editaddress({{$address->id ?? ''}})">
                                                            <i class="fas fa-edit text-secondary"></i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <span class="text-dark font-weight-bolder">آدرس:  &nbsp;</span>
                                                            <span class="text-secondary">{{$address->address ?? ''}}</span>
                                                        </td>
                                                        <td>
                                                            <span class="text-dark font-weight-bolder">کدپستی:  &nbsp;</span>
                                                            <span class="text-secondary"> {{$address->zipcode ?? ''}} </span>
                                                        </td>
                                                        <td class="table-address-remove text-center"
                                                            @click="deladdress({{$address->id ?? ''}})">
                                                            <i class="fa fa-remove text-secondary"></i>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            @endif
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row text-center">
                    <button id="stat" class="btn btn-outline-primary mx-auto stat"
                            @click="userstat" title="در ابتدا آدرس مرجع را گزینش کنید"
                            disabled>انتخاب آدرس مرجع
                    </button>
                </div>
                @includeif('partials.address')

            </div>
        </div>
    </div>
    </div>

@stop
