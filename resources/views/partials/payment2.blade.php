<div class="row">
    <div class=" col-sm-6">

        <h4 class="p-2 mr-5 pr-5">آدرس محل تحویل سفارش خودرا انتخاب و یا آدرس جدید اضافه کنید</h4>

    </div>
    <div class=" col-sm-5">

        <a hef="#" class="btn btn-default btn-shopping-address align-middle d-flex mx-auto text-dark"
           data-toggle="modal" data-target="#myAddressModal">
            <i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp;&nbsp;افزودن آدرس جدید</a>
    </div>
    <div class=" col-sm-1"></div>
</div>
<br>
<div class="row">
    <div class="col-sm-1"></div>
    <div class=" col-sm-10">
        @foreach($addresses as $address)
            @if(checkAddress($address))
                <table class="table border table-address">
                    <tbody>
                    <tr>
                        <td rowspan="2" class="table-address-radio text-center">
                            <input type="radio" class="mt-4" name="which" value="{{$address->id}}" onclick="enable()">
                        </td>
                        <td>
                            <span class="text-dark font-weight-bolder">نام تحویل گیرنده:  &nbsp;</span>
                            <span class="text-secondary">  {{$address->name}}</span>

                        </td>
                        <td>
                            <span class="text-dark font-weight-bolder">شماره تماس ثابت:  &nbsp;</span>
                            <span class="text-secondary"> {{$address->phone}} </span>
                        </td>
                        <td>
                            <span class="text-dark font-weight-bolder">شماره تماس موبایل:  &nbsp;</span>
                            <span class="text-secondary"> {{$address->mobile}} </span>
                        </td>
                        <td class="table-address-edit text-center" data-toggle="modal" data-target="#myAddressModal"
                            @click="editaddress({{$address->id}})">
                            <i class="fas fa-edit text-secondary"></i>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="text-dark font-weight-bolder">آدرس:  &nbsp;</span>
                            <span class="text-secondary">{{$address->address}}</span>
                        </td>
                        <td>
                            <span class="text-dark font-weight-bolder">کدپستی:  &nbsp;</span>
                            <span class="text-secondary"> {{$address->zipcode}} </span>
                        </td>
                        <td class="table-address-remove text-center" @click="deladdress({{$address->id}})">
                            <i class="fa fa-remove text-secondary"></i>
                        </td>
                    </tr>
                    </tbody>
                </table>
            @endif
        @endforeach
        <span> هزینه ارسال  <span class="site-green">رایگان</span>  تحویل اکپرس مزون نپال (ارسال رایگان برای خرید بالای 150 هزار تومان) </span>
        <br><br>
        <span class="px-5">
            <h5>زمان تحویل سفارشات:</h5>
            <p>زمان تقریبی تحویل سفارشات 24 ساعت کاری</p><br>
         </span>
        @includeif('partials.address')
    </div>
    <div class=" col-sm-1"></div>
</div>
<br>
<div class="row ">
    <div class="col-sm-12 ">
        <img src="/images/main/icon-pro.jpg" class="w-25 d-flex mx-auto opacity-50">
    </div>
</div>
<br><br>
<div class="row">
    <div class="col-sm-1 basket">
        <div class="clock">
            <div class="clock-hand clock-hand-short"></div>
            <div class="clock-hand clock-hand-long"></div>
        </div>
    </div>
    <div class="col-sm-6">
        <p class="basket-message">
            موجودی کالا <span style="color:#ef5a88">محدود </span>می باشد.
            افزودن محصولات به سبد به معنی رزرو آنها برای شما نیست.
            <br>
            برای حذف نشدن محصول از سبد خرید، لطفا سفارش خود را تکمیل نمایید. </p>
    </div>
    <div class="col-sm-4">
        <button class="btn btn-lg btn-primary next-cart2"
                data-target="#step3" data-toggle="tab" aria-controls="step3"
                role="tab" title="درابتدا آدرس را انتخاب کنید" @click="addstat" disabled> ثبت سبد و مرحله بعد >
        </button>
    </div>
</div>