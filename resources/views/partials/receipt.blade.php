<br><br>
<div class="row bg-light">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
        <div class="shadow-sm rounded p-3 p-sm-4 mb-4 bg-white my-5 mx-3">
            <div class="text-center my-5">
                <p class="text-center text-success text-20 line-height-07">
                    <i class="fas fa-check-circle"></i>
                </p>
                <p class="text-center text-success text-8 line-height-07">با موفقیت</p>
                @if(Session::get('type') =='real')
                    <p class="text-center text-4 left">سفارش ثبت گردید. </p>
                @else
                    <p class="text-center text-4 left">پرداخت انجام شد.</p>
                @endif

            </div>
            <p class="text-center text-3 mb-4 left">
                فاکتور شما با شماره
                <span class="text-4 text-success">{{Session::get('id_orders')}}</span>
                و کد رهگیری
                <span class="text-success">{{Session::get('order_id')}}</span>
                برای شما صادر گردید.
            </p>
            <button class="btn btn-success btn-block" style="padding: .8rem 2rem"> بازگشت به صفحه اصلی سایت</button>
            <button class="btn btn-link btn-block text-dark" onclick="window.print();return false;"><i
                        class="fas fa-print"></i> پرینت
            </button>
        </div>
    </div>
    <div class="col-sm-4"></div>

</div>
<div class="row bg-white" style="min-height: 150px"></div>
