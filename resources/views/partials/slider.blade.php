<div class="cycle-slideshow pics"
     data-cycle-fx="scrollHorz"
     data-cycle-swipe=true
     data-cycle-speed=2000
     data-cycle-timeout=4000>
    <div class="cycle-pager"></div>
    <div class="cycle-prev"></div>
    <div class="cycle-next"></div>
    @foreach($slides as $slide)
        <img class="img-fluid" src="{{$slide->image}}"/>
    @endforeach
</div>

