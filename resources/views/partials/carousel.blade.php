<div id="carousel">
    @foreach ($products as $product)
        <div id="pro" class="d-inline-block ">
            @php checkDiscount($discounts, $product)@endphp
            @php checkProduct($product) @endphp
            <h5 class="text-center bg-light p-3 "> {{$product->name}}</h5>
            <p class="pro-desc" title="{{$product->description}}"> {{$product->description}}</p>
            @php calDiscount($discounts, $product)@endphp
        </div>

    @endforeach

</div>
<a id="prev2" class="prev">&laquo; </a>
<a id="next2" class="next"> &raquo;</a>

