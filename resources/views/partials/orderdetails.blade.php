
@foreach($orders as $order)

    <!-- Modal -->
    <div id="myOrder{{$order->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog  modal-xl">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <button type="button" class="close-pro-modal" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <div class="col-md-11 main mx-auto d-block mb-3">
                        <div class="card ">
                            <div class="card-header text-center">
                                <h3>محصولات خریداری شده فاکتور شماره {{$order->id}} </h3>
                            </div>
                            <div class="card-body text-center">
                                <table class="table table-bordered table-hover table-responsive">
                                    <thead class="bg-info">
                                    <tr>
                                        <th>نام کالا</th>
                                        <th>تعداد</th>
                                        <th>برند کالا</th>
                                        <th>عکس محصول</th>
                                        <th>قیمت</th>
                                        <th>تعداد</th>
                                        <th>آدرس</th>
                                        <th>استان</th>
                                        <th>شهر</th>
                                        <th>شماره تماس</th>
                                    </tr>
                                    </thead>
                                    <tbody class="text-center bg-secondary">
                                    @foreach($order->products->unique() as $product)

                                        <tr>
                                            <td class="table p-2">
                                                {{$product->name}}
                                            </td>
                                            <td class="table p-2">
                                                {{countPro($order->id,$product->id)}}
                                            </td>
                                            <td class="table">
                                                {{showBrandByPro($product->brands)}}
                                            </td>
                                            <td class="table">
                                                <img src=" {{showImageByPro($product)}}" width='100px'>

                                            </td>
                                            <td class="table">
                                                {{$product->price}}
                                            </td>
                                            <td class="table">
                                                {{$product->name}}
                                            </td>
                                            <td class="table">
                                                {{$order->address->address}}
                                            </td>
                                            <td class="table">
                                                {{showStateByPro($order->address->province)}}
                                            </td>
                                            <td class="table">
                                                {{showCityByPro($order->address->city)}}
                                            </td>
                                            <td class="table">
                                                {{$order->address->mobile}}
                                                <hr>
                                                {{$order->address->phone}}
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer text-center "></div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer"></div>
            </div>

        </div>
    </div>
@endforeach