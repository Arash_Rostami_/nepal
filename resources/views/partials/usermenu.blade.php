<br><br>
<div class="row left">
    <div class="col-sm-1"></div>
    <div class="col-sm-1 text-right">
        <img src="/images/users/user-profile.png" width="75px">
    </div>
    <div class="col-sm-2 text-right">
        <h4>{{$user->name}}</h4>
        <h6>{{$user->email}}</h6>
    </div>
    <div class="col-sm-2"></div>
    <div class="col-sm-3 text-center">
        <p>تعداد سفارشات تایید شده</p>
        {{maxOrderConfirmed($user->id)}}
    </div>
    <div class="col-sm-3 text-center border-right">
        <p>تعداد کل سفارشات</p>
        {{maxOrder($user->id)}}

    </div>
</div>
<hr>

<div class="row left">
    <div class="col-sm-4">
        <div class="row my-3">
            <div class="col-md-2"></div>
            <div class="col-md-8 cat-menu text-right left h-auto">
                <div id="accordion">
                    <div class="card ">
                        <div class="card-header profile-card-menu">
                            <a class="card-link text-light" data-toggle="collapse" href="#collapseOne">
                                حساب کاربری
                            </a>
                        </div>
                        <div id="collapseOne" class="collapse show" data-parent="#accordion">
                            <div class="card-body bg-light">
                                <ul>
                                    <li class="my-1">اطلاعات مشتری</li>
                                    <ul>
                                        <li class="my-1">
                                            <a class="text-decoration-none" href="/user/profile/{{$user->id}}">مشخصات
                                                ثبت شده </a></li>
                                        <li class="my-1">
                                            <a class="text-decoration-none" href="/user/update/{{$user->id}}">ویرایش
                                                اطلاعات</a></li>
                                        <li class="my-1">
                                            <a class="text-decoration-none"
                                               href="/user/address/{{$user->id}}">آدرس </a></li>

                                    </ul>
                                    <li class="my-1">
                                        <a class="text-decoration-none"
                                           href="/user/order/{{$user->id}}">سفارشات </a></li>
                                    <li class="my-1"><a
                                                class="text-decoration-none" href="/user/list/{{$user->id}}">
                                            لیست خرید </a></li>
                                    <li class="my-1">
                                        <a class="text-decoration-none" href="/user/comment/{{$user->id}}">
                                            نظرات من</a></li>

                                    <li class="my-1">
                                        <a class="text-decoration-none" href="/user/question/{{$user->id}}">
                                            پرسش مستقیم </a></li>

                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>