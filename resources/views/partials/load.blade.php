<div class="row left p-2 my-3" onmouseenter="showSliderPro({{$product->id}})">
    <div class="col-sm-2 p-4 first-col">
        <a id="prev2" class="pre-arrow p-5"><i class="fa fa-chevron-up"></i> </a>
        <a id="next2" class="next-arrow p-5"><i class="fa fa-chevron-down"></i></a>
        <div id="mycarousel{{$product->id}}">
            @foreach($product->images as $image)
                <img src="{{$image->imagepath}}"
                     width="120px"
                     class="mt-4 mr-4 select">
            @endforeach
        </div>
    </div>

    <div class="col-sm-4 p-4 text-center second-col">
        <div class="row">
            <div class='col-sm-12' id="extension{{$product->id}}">
                <img src="{{$image->imagepath}}" class="zoomPro">
            </div>
        </div>
        <div class="row brand-logo-pro">
            <div class="col-sm-12 text-center p-2 mt-2">
                @foreach($product->brands as $brand)
                    <img src="{{$brand->imagepath}}" width="100px">
                @endforeach
            </div>
            @foreach($product->discounts as $discount)
                @if($discount->value)
                    <span class="discounted-rate"> {{discountRate($product)}}</span>
                    <span class="fa-stack fa-lg discounted-back">
                   <i class="fa fa-certificate fa-stack-2x text-secondary"></i>
                 </span>
                @endif
            @endforeach
        </div>
    </div>

    <div class="col-sm-4 text-right p-4">
        <div class="">
            @foreach($product->brands as $brand)
                <h1>{{$brand->name}}</h1>
            @endforeach
            <p>  {{$product->name}} </p>
            <div class="d-inline-flex">
                <h4 class='d-inline' style="color:#43C8D8">
                    سایز:</h4>
                <div class="p-2 mr-2">
                    <select class="form-control right niceSelectJ proSize" onchange="enableCart()">
                        <option data-display="گزینش کنید "></option>
                        @foreach($product->sizes as $size)
                            <option data-display="{{$size->size}}"
                                    value="{{$size->id}}">
                                {{$size->size}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <br><br>
            {{writeDiscount($product)}}
            @foreach($product->discounts as $discount)
                @if($discount->enddate >= thisMoment())
                    <span class="discounted-sum mr-1 mt-2">  {{ discountedSum($product)}}   تخفیف
                     <span class="fa-stack fa-lg">
                              <i class="fa fa-certificate fa-stack-2x "></i>
                              <i class="fa fa-tag fa-stack-1x fa-inverse text-secondary"></i>
                            </span>
                     @endif
                        @endforeach
                 </span>
                    <br>
                    <hr>
                        @if(Auth::guest())
                            <a role="button"
                               class="purchase-button shadow btn btn-lg purchase-button-model"
                               href="/logout"
                               title="در ابتدا وارد حساب کاربری شوید">
                                افزودن به سبد
                                خرید <br>
                                <i class="fa fa-cart-plus"></i>
                            </a>
                        @elseif(Auth::user())
                            <button class="purchase-button shadow btn btn-lg"
                                    data-toggle="modal" data-target="#myCart" style="cursor: no-drop;"
                                    title="در ابتدا سایز مورد دلخواه کالا را گزینش نمایید"
                                    @click="addToCart({{$product->id}})" disabled >
                                افزودن به سبد
                                خرید <br>
                                <i class="fa fa-cart-plus"></i>
                            </button>
                            @include('partials.cart')
                        @endif

                    <a href="#" data-toggle="tooltip" title="افزودن به لیست خرید بعدی" data-placement="bottom">
                        <i class="fa fa-heart-o p-2 text-secondary" style="font-size: 36px;"></i>
                    </a>
                    <br>
                    <img src="/images/main/icon-pro.jpg"
                         class="icon-product mt-4">
                    <div class="d-inline-block">
                        <p class="d-inline-block text-black-50"> هفت روز استرداد</p>
                        <p class="d-inline-block text-black-50 mr-2">ضمانت کیفیت کالا</p>
                        <p class="d-inline-block text-black-50 mr-2"> تحویل فوری کالا</p>
                    </div>
                    <br>
        </div>
        <div>
            <h5 class="text-secondary">ویژگی ها:</h5>

            <p class="text-secondary text-justify">
                {{$product->description}}
            </p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6 text-center left">
        <ul class="nav nav-tabs nav-justified">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#tabOne">مشخصات محصول</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tabTwo"> نظرات کاربران</a>
            </li>

        </ul>


        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane container" id="tabOne">
                <table class="table table-striped">
                    {{fetchTable($product)}}
                </table>
            </div>
            <div class="tab-pane container fade bg-secondary text-light" id="tabTwo">
                <p class="text-right">لطفا قبل از نوشتن نظر خود درباره این محصول، وارد حساب کاربری خود شوید.</p>
                <form action="/addcomment" method="POST">
                    @csrf
                    <div class=" text-right">
                        <h6 class='d-inline-flex text-secondary text-right'>
                            سایز:</h6>
                        <div class="p-2 mr-2 col-sm-3 d-inline-flex">
                            <select class="form-control right" name="size" required>
                                <option data-display="گزینش کنید "></option>
                                @foreach($product->sizes as $size)
                                    <option data-display="{{$size->size}}"
                                            value="{{$size->id}}">
                                        {{$size->size}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group  text-right">
                        <label>عنوان نظر:</label>
                        <input type="text"
                               class="form-control" name="title" id="" aria-describedby="helpId"
                               placeholder="عنوان نظر خود را وارد نمایید ..." required>
                    </div>
                    <div class="form-group text-right ">
                        <label>متن نظر: </label>
                        <textarea class="form-control mb-3 pb-" name="comment" id="" rows="6" required></textarea>
                    </div>
                    <div class="form-check-inline text-right">
                        <label class="form-check-label" for="check1">من این محصول را پیشنهاد می کنم
                            <input type="radio" class="form-check-input" id="check1" name="suggestion" value="yes"
                                   checked>
                        </label>
                    </div>
                    <div class="form-check-inline">
                        <label class="form-check-label" for="check2">من این محصول را پیشنهاد نمی کنم
                            <input type="radio" class="form-check-input" id="check2" name="suggestion" value="no">
                        </label>
                        <input type="hidden" name="user" value="{{session('user')}}">
                        <input type="hidden" name="product" value="{{$product->id}}">
                        <input type="hidden" name="group" value="{{$product->group_id}}">
                    </div>
                    <div class="text-left">
                        <button type="submit" class="btn btn-success mb-3 pb-3">ارسال نظر</button>
                    </div>
                </form>
                <hr>
                <p class="text-right">نظرات نوشته شده:</p>
                {!! showProductComments($product) !!}
            </div>

        </div>
    </div>
    <div class="col-sm-3"></div>
</div>
@include('main.signup')
<div class="row my-4">
    <div class="col-sm-1"></div>
    <div class="col-sm-10">
        <h4 class="text-center p-5 d-none" id="others">دیگر محصولات مرتبط با این کالا</h4>
        <div id="carousel">
            {{showOtherProducts($groups, $product, $discounts)}}
        </div>
    </div>
    <div class="col-sm-1"></div>
</div>




