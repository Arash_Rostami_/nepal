@extends('admin.profile')

@section('profile')
    <div class="col-sm-8">
        <div class="row my-3 ml-5">
            <div class="col-md-12 cat-menu text-right h-auto">
                <div id="accordionTwo">
                    <div class="card ">
                        <div class="card-header profile-card-menu">
                            <a class="card-link text-light" data-toggle="collapse" href="#collapseTwo">
                                ارسال پرسش
                            </a>
                        </div>
                        <div id="collapseTwo" class="collapse show" data-parent="#accordionTwo">
                            <div class="card-body bg-light">
                                <form action="submit" method="post" id="form1">
                                    @csrf
                                    <label>
                                        سوال خود را برای ما ارسال کرده تا در اسرع وقت به آن پاسخ دهیم.
                                    </label>
                                    <textarea class="form-control" name="question" rows="10" onfocus="enableSubmit()">
                                    </textarea>
                                    <input type="hidden" value="{{$user->id}}" name="user">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row text-center">
                    <input type="submit" class="btn btn-outline-primary mx-auto quest" value="ارسال سوال"
                           title="در ابتدا سوال را بنوسید" form="form1" disabled>
                </div>

            </div>
        </div>
    </div>

    </div>

@stop
