<h4 class="p-2 mr-5 pr-5">سبد خرید شما</h4>
<div class="row pl-5">
    <div class="col-sm-12 px-5">
        <table class="table shopping-table">
            <thead>
            <tr>
                <th>مشخصات محصول</th>
                <th>تعداد</th>
                <th>قیمت</th>
            </tr>
            </thead>
            <tbody>
            @if(!empty(Session::get('pro')))
                @foreach(array_unique(Session::get('pro')) as $key=>$value)
                    <tr>
                        <td>
                            <br>
                            {{getProductName($value)}}
                            <br>
                            <img src=" {{getProductImage($value)}}" width="120px">
                            <br>
                            <p>کد کالا: P{{$value}}</p>
                            <p>سایز</p>
                            <h5>{{getProductSize($sizeO['product' . $value])}}</h5>
                            <h5 class="invisible proSize{{$value}}">{{$sizeO['product' . $value]}}</h5>
                        </td>
                        <td>
                            <div class="item-counter p-5">
                                <i class="fa fa-plus d-inline pl-2" @click="addToCartEdit({{$value}})"></i>
                                <input type="number" id="item{{$value}}" class=" d-inline" size="12"
                                       value="@php print_r(count(array_keys(Session::get('pro'), $value))) @endphp"
                                       min="0" max="5">
                                <i class="fa fa-minus d-inline pr-2"
                                   @click="removeFromCartEdit({{$key}}, {{$value}})"></i>
                                <br><br>
                            </div>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-sm-4 pt-5">
                                    <p class="d-inline pt-5">قیمت واحد کالا:</p>
                                </div>
                                <div class="col-sm-6 pt-5">
                                    <p class="d-inline pt-5 mr-5">{{number_format(getProductPrice($value))}} تومان</p>
                                </div>
                                <div class="col-sm-2 closing-cross align-middle text-center">
                                    <i class="fa fa-remove pt-5 mt-5 text-secondary"
                                       @click="removeAllFromCart({{$key}}, {{$value}})"></i>
                                </div>
                            </div>
                            <hr class="p-0 m-0">
                            <div class="row pt-0 mt-0 pb-3">
                                <div class="col-sm-4">
                                    <p class="d-inline">قیمت کل:</p>
                                </div>
                                <div class="col-sm-6">
                                    <p class="d-inline pt-5 mr-5">
                                        {{number_format(getProductPrice($value)*count(array_keys(Session::get('pro'), $value)))}}
                                        تومان</p>
                                </div>
                                <div class="col-sm-2">
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
<br>
<div class="row ">
    <div class="col-sm-6 mr-5">
        <img src="/images/main/icon-pro.jpg" class="icon-pro">
    </div>
    <div class="col-sm-5 bg-light ml-2 p-2">
        <h5 class="mt-4 mr-5"> مبلغ نهایی فاکتور: </h5>
        <h4 class="text-center bg-dark text-light">{{number_format(Session::get('shopCart.total'))}} تومان
        </h4>
    </div>
</div>
<br><br>
<div class="row">
    <div class="col-sm-1 basket">
        <div class="clock">
            <div class="clock-hand clock-hand-short"></div>
            <div class="clock-hand clock-hand-long"></div>
        </div>
    </div>
    <div class="col-sm-6">
        <p class="basket-message">
            موجودی کالا <span style="color:#ef5a88">محدود </span>می باشد.
            افزودن محصولات به سبد به معنی رزرو آنها برای شما نیست.
            <br>
            برای حذف نشدن محصول از سبد خرید، لطفا سفارش خود را تکمیل نمایید. </p>
    </div>
    <div class="col-sm-4">
        <button class="btn btn-lg btn-primary next-cart1"
                data-target="#step2" data-toggle="tab" aria-controls="step2"
                role="tab" title="مرحله دوم"> ثبت سبد و مرحله بعد >
        </button>
    </div>
</div>
<br>