<div class="container-large">
    <ul class="nav d-inline d-flex justify-content-center flex-row-reverse">
        @foreach($parents as $parent)
            <li class=" d-inline m-4">
                <a href="#" class="color-link dropdown dropdown-large dropdown-toggle-split text-decoration-none"
                   data-toggle="dropdown">
                    {{ $parent->name }}
                </a>
                <div class="dropdown-menu dropdown-menu-large left mx-sm-5 p-sm-3 menu-box" role="menu">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="row">
                                @foreach($categories as $category)
                                    @if($parent->id == $category->parent_id )
                                        <div class="col-sm-2 mx-4 mt-3">
                                            <a class="dropdown-item border-bottom color-link"
                                               href="/filter/{{$category->id}}">
                                                <h5 class="color-link menu-fontsize text-primary
                                                 font-weight-bolder">
                                                    {{$category->name}}</h5>
                                            </a>
                                            @foreach($groups as $group)
                                                @if($group->category_id == $category->id )
                                                    <a href="/filter/{{$category->id}}" class="text-decoration-none dropdown-item color-link
                                 menu-fontsize">{{$group->name}}</a>
                                                @endif
                                            @endforeach
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="col-sm-2 brand">
                            <div class="row">
                                @foreach( $brands as $brand)
                                    @if($brand->parent_id == $parent->id)
                                        <div class="brand-cover-menu col-sm-4 border p-sm-1 m-sm-2 text-center mb-3">
                                            <img class="image-brand-menu image" src="{{$brand->imagepath}}">
                                            <div class="overlay">
                                                <div class="text">{{$brand->name}}
                                                </div>
                                            </div>
                                        </div>

                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="col-sm-3 smallBanner">
                            @php   checkBanner($parent) @endphp

                        </div>
                    </div>

                </div>
            </li>
        @endforeach
    </ul>

</div>
