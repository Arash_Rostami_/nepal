@extends('admin.profile')

@section('profile')

    <div class="col-sm-8">
        <div class="row my-3 ml-5">
            <div class="col-md-12 cat-menu text-right h-auto">
                <div id="accordionTwo">
                    <div class="card ">
                        <div class="card-header profile-card-menu">
                            <a class="card-link text-light" data-toggle="collapse" href="#collapseTwo">
                                نظرات شما
                            </a>
                        </div>
                        <div id="collapseTwo" class="collapse show" data-parent="#accordionTwo">
                            <div class="card-body bg-light">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>محصولی که نظر برای آن ثبت شده</th>
                                        <th> وضعیت</th>
                                        <th>نظر</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($comments as $comment)
                                        <tr>
                                            <td style="width:35%">
                                                {{$comment->product->name .' - سایز' .$comment->sizes->size ?? ''}}</td>
                                            <td style="width:15%">
                                                @isset($comment->status)
                                                    {{($comment->status==1) ? 'انتشار یافته' : 'در حال بررسی'}}</td>
                                            @endisset
                                            <td style="width:50%">{{$comment->comment ?? ''}}</td>

                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                                {{$comments->render()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>

@stop
