<div class="col-md-10 main mx-auto d-block">
    <div class="card ">
        <div class="card-header text-center">
            <h3>مشاهده نظرات</h3>
        </div>
        <div class="card-body text-center">
            <table class="table table-bordered table-hover table-responsive">
                <thead>
                <tr>
                    <th>متن</th>
                    <th>شماره کامنت</th>
                    <th>تیتز</th>
                    <th>ایمیل</th>
                    <th>گروه کالایی</th>
                    <th>محصول</th>
                    <th>تاریخ</th>
                    <th>وضعیت</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>

                @foreach($comments as $comment)
                    <tr>
                        <td class="table p-2" style="width:10%">{{$comment->comment }}</td>
                        <td class="table" style="width:10%">{{$comment->id}}</td>
                        <td class="table" style="width:10%">{{$comment->title}}</td>
                        <td class="table" style="width:10%">{{$comment->user->email ?? ''}}</td>
                        <td class="table" style="width:10%">{{$comment->group->name ??''}}</td>
                        <td class="table" style="width:10%">{{$comment->product->name ?? ''}}</td>
                        <td class="table" style="width:10%">{{$comment->date}}</td>
                        <td class="table text-center" @click="changeStatus({{$comment->id}}, {{$comment->status}})">
                            @if($comment->status == 0)
                                <i class="far fa-times-circle btn btn-danger text-light"></i>
                            @else
                                <i class="fas fa-check-circle btn btn-success text-light"></i>
                            @endif
                        </td>
                        <td class="table" style="width:10%">
                            <input type="submit" class="btn btn-danger"
                                   value="حذف" @click="deleteComment({{$comment->id}})">
                            @csrf()
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>

        </div>
        <div class="card-footer text-center ">
            <h6>{{$comments->links()}}</h6>
        </div>
    </div>
</div>

<div class="col-md-10 main mx-auto d-block mb-3">
    <div class="card ">
        <div class="card-header text-center">
            <h3>مشاهده پاسخ نظرات</h3>
        </div>
        <div class="card-body text-center">
            <table class="table table-bordered table-hover table-responsive">
                <thead>
                <tr>
                    <th>متن</th>
                    <th>شماره کامنت</th>
                    <th>مرتبط</th>
                    <th>ایمیل</th>
                    <th>تاریخ</th>
                    <th>وضعیت</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($responses as $response)
                    <tr>
                        <td class="table p-2" style="width:10%">{{$response->comment}}</td>
                        <td class="table" style="width:10%">{{$response->id}}</td>
                        <td class="table" style="width:10%">{{$response->title}}</td>
                        <td class="table" style="width:10%">{{$response->user->email}}</td>
                        <td class="table" style="width:10%">{{$comment->date}}</td>
                        <td class="table text-center" style="width:10%"
                            @click="changeStatusRes({{$response->id}}, {{$response->status}})">
                            @if($response->status == 0)
                                <i class="far fa-times-circle btn btn-danger text-light"></i>
                            @else
                                <i class="fas fa-check-circle btn btn-success text-light"></i>
                            @endif
                        </td>
                        <td class="table" style="width:10%">
                            <input type="submit" class="btn btn-danger"
                                   value="حذف" @click="deleteCommentRes({{$response->id}})">
                            @csrf()
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer text-center ">
            <h6>{{$responses->links()}}</h6>
        </div>
    </div>
</div><br>
