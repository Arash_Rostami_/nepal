@extends('admin.profile')

@section('profile')

    <div class="col-sm-8">
        <div class="row my-3 ml-5">
            <div class="col-md-12 cat-menu text-right h-auto">
                <div id="accordionTwo">
                    <div class="card ">
                        <div class="card-header profile-card-menu">
                            <a class="card-link text-light" data-toggle="collapse" href="#collapseTwo">
                                اطلاعات مشتری
                            </a>
                        </div>
                        <div id="collapseTwo" class="collapse show" data-parent="#accordionTwo">
                            <div class="card-body bg-light">
                                <table class="table">
                                    <tbody>
                                    <tr class="bg-light">
                                        <td> نام و نام خانوادگی:
                                            <span class="text-user-info">{{ (' '.$user->name. ' ' .$user->lname) ?? '' }} </span>
                                        </td>
                                        <td>تاریخ تولد:
                                            <span class="text-user-info">{{ ' '.$user->birthdate ?? '' }} </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>آدرس اکترونیک:
                                            <span class="text-user-info">{{ ' '.$user->email}} </span>
                                        </td>
                                        <td>جنسیت:
                                            @isset($user->gender)
                                                <span class="text-user-info">{{ ($user->gender==1? 'مرد': 'زن') }} </span>
                                            @endisset
                                        </td>
                                    </tr>
                                    <tr class="bg-light">
                                        <td>کد ملی:
                                            <span class="text-user-info">{{ ' '.$user->idcode ?? ''}} </span>
                                        </td>
                                        <td>محل سکونت:
                                            <span class="text-user-info">{{ ' '. userAddress($user->id)->value('address') ?? ''}} </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>شماره موبایل:
                                            <span class="text-user-info">{{ ' '. userAddress($user->id)->value('mobile') ?? ''}} </span>
                                        </td>
                                        <td>شماره تلفن ثابت:
                                            <span class="text-user-info">{{ ' '. userAddress($user->id)->value('phone') ?? ''}} </span>
                                        </td>
                                    </tr>
                                    <tr class="bg-light">
                                        <td>شماره کارت بانکی:
                                            <span class="text-user-info">{{ ' '.$user->accountno ?? ''}} </span>
                                        </td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <a href="/user/update/{{$user->id}}" class="btn btn-outline-primary mx-auto"
               role="button" aria-disabled="true">ویرایش اطلاعات</a>
        </div>
    </div>
    </div>

@stop
