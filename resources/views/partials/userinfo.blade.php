@extends('admin.profile')

@section('profile')


    <div class="col-sm-8">
        <div class="row my-3 ml-5">
            <div class="col-md-12 cat-menu text-right h-auto">
                <div id="accordionTwo">
                    <div class="card ">
                        <form action="/user/ind" method="post" id="myForm" name="myForm">

                            <div class="card-header profile-card-menu">
                                <a class="card-link text-light" data-toggle="collapse" href="#collapseTwo">
                                    ویرایش اطلاعات
                                </a>
                            </div>
                            <div id="collapseTwo" class="collapse show" data-parent="#accordionTwo">
                                <div class="card-body bg-light">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab"
                                               href="#individual">حقیقی</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#corporation">حقوقی</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane container active" id="individual">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">نام</label>
                                                        <input type="hidden" name="id" value="{{AUth::id()}}">
                                                        <input type="text" name="fname" class="form-control"
                                                               placeholder="به فارسی" value="{{$user->name}}"
                                                               aria-describedby="helpId">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">نام خانوادگی</label>
                                                        <input type="text" name="lname" class="form-control"
                                                               placeholder="به فارسی" value="{{$user->lname}}"
                                                               aria-describedby="helpId">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" name="idcode"
                                                                      onclick="idCode(this)"
                                                                      value="0"> تبعه خارجی بوده و فاقد کد ملی
                                                            هستم</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label> جنسیت: </label><br>
                                                    <label> مرد </label><input type="radio" name="gender" value="1">
                                                    <br>
                                                    <label> زن </label><input type="radio" name="gender" value="0">
                                                </div>
                                            </div>
                                            <br>


                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">کد ملی</label>
                                                        <input type="text" name="idcode" class="form-control"
                                                               id="idcode" value="{{$user->idcode ?? ''}}"
                                                        >
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">تلفن همراه</label>
                                                        <input type="tel" name="mobile" class="form-control"
                                                               value="{{userAddress($user->id)->value('mobile') ?? '' }}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="">محل سکونت</label>
                                                        <select class="form-control" name="state"
                                                                onChange="CityList(this);">
                                                            <option value="0"> انتخاب نمایید</option>
                                                            <option value="1">تهران</option>
                                                            <option value="2">گیلان</option>
                                                            <option value="3">آذربایجان شرقی</option>
                                                            <option value="4">خوزستان</option>
                                                            <option value="5">فارس</option>
                                                            <option value="6">اصفهان</option>
                                                            <option value="7">خراسان رضوی</option>
                                                            <option value="8">قزوین</option>
                                                            <option value="9">سمنان</option>
                                                            <option value="10">قم</option>
                                                            <option value="11">مرکزی</option>
                                                            <option value="12">زنجان</option>
                                                            <option value="13">مازندران</option>
                                                            <option value="14">گلستان</option>
                                                            <option value="15">اردبیل</option>
                                                            <option value="16">آذربایجان غربی</option>
                                                            <option value="17">همدان</option>
                                                            <option value="18">کردستان</option>
                                                            <option value="19">کرمانشاه</option>
                                                            <option value="20">لرستان</option>
                                                            <option value="21">بوشهر</option>
                                                            <option value="22">کرمان</option>
                                                            <option value="23">هرمزگان</option>
                                                            <option value="24">چهارمحال و بختیاری</option>
                                                            <option value="25">یزد</option>
                                                            <option value="26">سیستان و بلوچستان</option>
                                                            <option value="27">ایلام</option>
                                                            <option value="28">کهگلویه و بویراحمد</option>
                                                            <option value="29">خراسان شمالی</option>
                                                            <option value="30">خراسان جنوبی</option>
                                                            <option value="31">البرز</option>
                                                        </select>
                                                        <small id="helpId" class="text-muted" required>استان</small>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="invisible"> شهر</label>
                                                        <select class="d-inline form-control" name="city" id="city"
                                                                required>
                                                            <option value=""> ابتدا استان انتخاب کنید</option>
                                                        </select>
                                                        <small id="helpId" class="text-muted">شهر</small>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <label for="">تاریخ تولد</label>
                                                        <input type="number" name="dbirth" class="form-control"
                                                               aria-describedby="helpId">
                                                        <small id="helpId" class="text-muted">روز</small>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <label class="invisible">ماه</label>
                                                        <input type="number" name="mbirth" class="form-control"
                                                               aria-describedby="helpId">
                                                        <small id="helpId" class="text-muted">ماه</small>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <label class="invisible">سال</label>
                                                        <input type="number" name="ybirth" class="form-control"
                                                               aria-describedby="helpId">
                                                        <small id="helpId" class="text-muted">سال</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="">آدرس</label>
                                                        <input type="text" name="address" class="form-control"
                                                               placeholder="بدون درج نام استان یا شهر"
                                                               value="{{userAddress($user->id)->value('address') ?? ''}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for=""> کد پستی</label>
                                                        <input type="text" name="zipcode" class="form-control"
                                                               aria-describedby="ziphelpId"
                                                               value="{{userAddress($user->id)->value('zipcode') ?? ''}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">تلفن ثابت</label>
                                                        <input type="text" name="phone" class="form-control"
                                                               value="{{userAddress($user->id)->value('phone') ?? ''}}"
                                                               aria-describedby="helpId">
                                                        <small id="helpId" class="text-muted">*اختیاری</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="">شماره کارت بانکی</label>
                                                        <input type="text" name="accountno" class="form-control"
                                                               aria-describedby="helpId"
                                                               value="{{$user->accountno ?? ''}}">
                                                        <small id="helpId"
                                                               class="text-muted text-center bg-warning"> * شماره
                                                            کارت بانکی 16 رقمی تنها برای استرداد مبلغ پرداختی در
                                                            زمانهای مورد
                                                            نیاز می باشد و درج آن اجباری نیست *
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane container fade" id="corporation">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">نام شرکت</label>
                                                        <input type="text" name="cname" class="form-control"
                                                               placeholder="به فارسی" value="{{$company->name ?? ''}}"
                                                               aria-describedby="helpId">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">کد اقتصادی </label>
                                                        <input type="text" name="cbnumber" class="form-control"
                                                               value="{{$company->code ?? ''}}"
                                                               aria-describedby="helpId">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">شناسه ملی</label>
                                                        <input type="text" name="cidcode" class="form-control"
                                                               id="idcode" value="{{$company->idcode ?? ''}}"
                                                        >
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">شماره ثبت </label>
                                                        <input type="tel" name="cregicode" class="form-control"
                                                               value="{{$company->number ?? ''}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="">محل شرکت</label>
                                                        <select class="form-control" name="cstate"
                                                                onChange="CityList(this);" required>
                                                            <option value="0"> انتخاب نمایید</option>
                                                            <option value="1">تهران</option>
                                                            <option value="2">گیلان</option>
                                                            <option value="3">آذربایجان شرقی</option>
                                                            <option value="4">خوزستان</option>
                                                            <option value="5">فارس</option>
                                                            <option value="6">اصفهان</option>
                                                            <option value="7">خراسان رضوی</option>
                                                            <option value="8">قزوین</option>
                                                            <option value="9">سمنان</option>
                                                            <option value="10">قم</option>
                                                            <option value="11">مرکزی</option>
                                                            <option value="12">زنجان</option>
                                                            <option value="13">مازندران</option>
                                                            <option value="14">گلستان</option>
                                                            <option value="15">اردبیل</option>
                                                            <option value="16">آذربایجان غربی</option>
                                                            <option value="17">همدان</option>
                                                            <option value="18">کردستان</option>
                                                            <option value="19">کرمانشاه</option>
                                                            <option value="20">لرستان</option>
                                                            <option value="21">بوشهر</option>
                                                            <option value="22">کرمان</option>
                                                            <option value="23">هرمزگان</option>
                                                            <option value="24">چهارمحال و بختیاری</option>
                                                            <option value="25">یزد</option>
                                                            <option value="26">سیستان و بلوچستان</option>
                                                            <option value="27">ایلام</option>
                                                            <option value="28">کهگلویه و بویراحمد</option>
                                                            <option value="29">خراسان شمالی</option>
                                                            <option value="30">خراسان جنوبی</option>
                                                            <option value="31">البرز</option>
                                                        </select>
                                                        <small id="helpId" class="text-muted">استان</small>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="invisible"> شهر</label>
                                                        <select class="d-inline form-control" name="ccity"
                                                                id="ccity" required>
                                                            <option value="0"> ابتدا استان انتخاب کنید</option>
                                                        </select>
                                                        <small id="helpId" class="text-muted">شهر</small>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2"></div>
                                                <div class="col-sm-2"></div>
                                                <div class="col-sm-2"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="">آدرس</label>
                                                        <input type="text" name="caddress" class="form-control"
                                                               placeholder="به فارسی"
                                                               value="{{$company->address ?? ''}}">
                                                        <small id="helpId" class="text-muted">بدون درج نام استان
                                                            یا
                                                            شهر
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">تلفن ثابت</label>
                                                        <input type="text" value="{{$company->phone ?? ''}}"
                                                               name="cphone"
                                                               class="form-control"
                                                               aria-describedby="helpId">
                                                    </div>
                                                </div>

                                                <div class="col-sm-6"></div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>

                    </div>


                </div>

            </div>

        </div>
        <div class="row">
            <input type="submit" class="btn btn-primary mx-auto" value="ویرایش اطلاعات" id="submitButton"
                   form="myForm">

        </div>
    </div>
    </form>

    </div>



@stop
