@if(Session::get('pro'))
<div class="modal fade" id="myCart">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-info text-center">
                <h5 class="text-light text-center"> کالای زیر به سبد شما اضافه خواهد شد</h5>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-9">
                        <table class="table table-bordered text-center shopping-cart">
                            <thead>
                            <tr>
                                <th>نام کالا</th>
                                <th>تعداد</th>
                                <th>قیمت</th>
                                <th>تصویر</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td> {{$product->name}}</td>
                                <td> 1</td>
                                <td> {{number_format($product->price)}} تومان</td>
                                <td><img src=" {{getProductImage($product->id)}} " width="50px"></td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table text-center">
                            <thead>
                            <tr>
                                <th> کل</th>
                                <th>1</th>
                                <th>{{number_format($product->price)}} تومان</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
@endif
@if(!empty(Session::get('shopCart')))
    <div class="modal fade" id="myCartList">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header bg-info">
                    <h4 class="text-light text-right"> اقلام افزوده شده به سبد خرید</h4>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-9">
                            <table class="table table-bordered text-center shopping-cart">
                                <thead>
                                <tr>
                                    <th>نام کالا</th>
                                    <th>تعداد</th>
                                    <th>قیمت</th>
                                    <th>تصویر</th>
                                    <th>حذف</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(Session::get('pro') as $key=>$value)
                                    <tr>
                                        <td> {{getProductName($value)}}</td>
                                        <td> 1</td>
                                        <td> {{number_format(getProductPrice($value))}} تومان</td>
                                        <td><img src=" {{getProductImage($value)}}" width="50px"></td>
                                        <td class="btnp">
                                            <i class="fas fa-trash-alt text-danger"
                                               style="font-size: large"
                                               @click="removeFromCart({{$key}}, {{$value}})"></i></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <table class="table text-center">
                                <thead>
                                <tr>
                                    <th> جمع کل اقلام</th>
                                    <th>{{Session::get('shopCart.qty')}}</th>
                                    <th>{{number_format(Session::get('shopCart.total'))}} تومان</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-sm-1"></div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer text-center bg-secondary text-light">
                    <a class="curs text-light text-decoration-none" href="/checkout/final/{{Auth::id()}}"> رفتن به مرحله
                        بعد >></a>
                </div>
            </div>
        </div>
    </div>
@endif

