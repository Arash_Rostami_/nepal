<div class="col-md-8 main mx-auto d-block">
    <div class="card ">
        <div class="card-header text-center">
            <h3>مشاهده کالاهای موجود</h3>
        </div>
        <div class="card-body text-center">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>نام کالا</th>
                    <th> قیمت</th>
                    <th> تخفیف</th>
                    <th>دسته کالا</th>
                    <th>تاریخ قرارگیری در سایت</th>
                    <th>مشاهده کالا</th>
                    <th>ویرایش کالا</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody class=" w-50 p-5">
                @foreach($products as $product)
                    <tr>
                        <td class="table">{{$product->name}}</td>
                        <td class="table w-auto">تومان {{$product->price}}</td>
                        <td class="table w-auto">{{$product->getDiscounts($product->id)}}</td>
                        <td class="table w-auto">{{$product->category_id}}</td>
                        <td class="table w-auto">{{Morilog\Jalali\Jalalian::forge($product->created_at)->format('%A, %d %B %y')}}</td>
                        <td class="table w-auto">
                            <input type="submit" class="btn btn-info" id="myProductModel"
                                   data-target="#productModel{{$product->id}}"
                                   data-toggle="modal" data-backdrop="true" value="مشاهده اطلاعات کالا">
                            <div class="modal fade right overflow-auto" id="productModel{{$product->id}}" tabindex="-1"
                                 role="dialog"
                                 aria-labelledby="modelTitleId" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content overflow-auto ">
                                        <div class="modal-header bg-info ">
                                            <button type="button" class="close-pro-des"
                                                    data-dismiss="modal"
                                                    aria-label="بستن">
                                                <span class="right" style="direction:ltr"
                                                      aria-hidden="true">&times;</span>
                                            </button>
                                            <br>
                                            <h5 class="modal-title"> جزییات {{$product->name}}</h5>
                                        </div>
                                        <div class="modal-body overflow-auto">
                                            <div class="row">
                                                <div class="col-sm-6 pt-2">
                                                    <h6>مشخصات</h6>
                                                    <table class="table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th>گروه کالایی</th>
                                                            <th>سایز</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="text-center">
                                                        @foreach($product->sizes as $size)
                                                            <tr>
                                                                <td>{{ $size->getGroup($size->group_id) }}</td>
                                                                <td>{{ $size->size }}</td>
                                                            </tr>

                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-sm-6 pt-2">
                                                    <h6>مشخصات محصول</h6>
                                                    <table class="table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th>نام</th>
                                                            <th>نوع</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="text-center">
                                                        @foreach($product->attributes as $attr)
                                                            <tr>
                                                                <td>{{ $attr->name }}</td>
                                                                <td>{{ $attr->getAttributeItem($attr->id) }}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-sm-6 pt-2">
                                                    <h6>برند محصول</h6>
                                                    <table class="table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th>نام</th>
                                                            <th>کشور</th>
                                                            <th>تصویر</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="text-center">
                                                        <tr>
                                                            <td>{{ $product->getBrandsName($product->id) }}</td>
                                                            <td>{{ $product->getBrandsCountry($product->id) }}</td>
                                                            <td><img src="{{ $product->getBrandsPhoto($product->id) }}"
                                                                     width="100px"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-sm-6 pt-2">
                                                    <h6 class="border-bottom">توضیحات کالا</h6>

                                                    {{ $product->description }}
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                {!! $product->getImages($product->id) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
        </div>
        </td>

        <td class="table w-auto">
            <input type="submit" class="btn btn-warning" id="myProductModel"
                   data-target="#productEdit{{$product->id}}"
                   data-toggle="modal" data-backdrop="true" value="ویرایش">
            <div class="modal fade right overflow-auto" id="productEdit{{$product->id}}" tabindex="-1"
                 role="dialog"
                 aria-labelledby="modelTitleId" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content overflow-auto ">

                        <div class="modal-header bg-info">
                            <h5 class="modal-title"></h5>
                            <button type="button" class="close-pro-modal" data-dismiss="modal"
                                    aria-label="بستن">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <form method="post" action="/admin/product/update">
                                        @csrf
                                        @method('patch')
                                        <div class="form-group">
                                            <label for="">نام کالا</label>
                                            <input type="text" class="form-control" name="name"
                                                   value="{{$product->name}}">
                                            <input type="text" class="form-control" id="idProTest" name="id"
                                                   value="{{$product->id}}">
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="">توضیحات</label>
                                            <textarea class="form-control" name="desc"
                                                      rows="6">{{$product->description}}</textarea>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for=""> قیمت محصول</label>
                                            <input type="text" class="form-control" name="price"
                                                   value="{{$product->price}}"><br>
                                            <hr>
                                            <input type="hidden" class="form-control" id="demoname"
                                            ><br>
                                        </div>
                                        <div class="form-group ">
                                            <label for=""> دسته بندی کالا</label>
                                            <select class="form-control" name="group">
                                                @foreach($groups as $group)
                                                    <option value="{{$group->id}}"
                                                            @php echo (($group->id ==$product->group_id )? "selected" :'' ); @endphp>
                                                        {{$group->name}}
                                                    </option>
                                                @endforeach
                                            </select><br>
                                            <input type="submit" value="ذخیره" class="btn btn-primary">
                                        </div>
                                    </form>


                                    <br>
                                    <div class="row">
                                        <label class="col-md-12 control-label"> عکس کالا</label>
                                    </div>
                                    <div class="row">
                                        <form action="/admin/product/update"
                                              class="dropzone col-md-10 mr-1 ml-3"
                                              id="dzEditProduct"
                                              method="post"
                                              enctype="multipart/form-data">
                                            @csrf
                                            @method('patch')

                                            <div class="fallback">
                                                <input name="file" type="file" multiple/>
                                            </div>
                                        </form>

                                    </div>

                                </div>
                                <div class="col-md-6 ">

                                    <form method="post" action="/admin/product/del">
                                        @csrf
                                        <div class="row">
                                            <label class="col-md-4 control-label"> سایز موجود </label>
                                            <div class="form-group col-md-6 ">
                                                <input type="hidden" class="form-control" name="prsize"
                                                       value="{{$product->id}}">
                                                <select class="form-control" name="size">
                                                    {{deleteItem($product, $sizes, 'sizes', 'size')}}
                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <input type="submit" value="حذف شود" class="btn btn-danger">
                                            </div>

                                        </div>
                                    </form>
                                    <br>
                                    <form method="post" action="/admin/product/show/add">
                                        @csrf
                                        <div class="row">
                                            <label class="col-md-4 control-label"> سایزهای اضافه</label>
                                            <div class="form-group col-md-6 ">

                                                <input type="hidden" class="form-control" name="prsize"
                                                       value="{{$product->id}}">
                                                <select class="form-control" name="size">
                                                    @php
                                                        fetchSort($sizes,'size', 'id')
                                                    @endphp
                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <input type="submit" value="اضافه شود" class="btn btn-success">
                                            </div>

                                        </div>
                                    </form>
                                    <hr>
                                    <form method="post" action="/admin/product/del">
                                        @csrf
                                        <div class="row">
                                            <label class="col-md-4 control-label"> عنوان خصوصیت موجود</label>
                                            <div class="form-group col-md-6 ">

                                                <input type="hidden" class="form-control" name="prattr"
                                                       value="{{$product->id}}">
                                                <select class="form-control" name="attr">
                                                    {{deleteItem($product, $attrs, 'attributes', 'name')}}
                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <input type="submit" value="حذف شود" class="btn btn-danger">
                                            </div>
                                        </div>
                                    </form>
                                    <br>
                                    <form method="post" action="/admin/product/show/add">
                                        @csrf
                                        <div class="row">
                                            <label class="col-md-4 control-label"> عنوان خصوصیت اضافه</label>
                                            <div class="form-group col-md-6 ">

                                                <input type="hidden" class="form-control" name="prattr"
                                                       value="{{$product->id}}">
                                                <select class="form-control" name="attr">
                                                    @php
                                                        fetchSort($attrs,'name', 'id')
                                                    @endphp

                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <input type="submit" value="اضافه شود" class="btn btn-success">
                                            </div>
                                        </div>
                                    </form>
                                    <hr>
                                    <form method="post" action="/admin/product/del">
                                        @csrf
                                        <div class="row">
                                            <label class="col-md-4 control-label"> مقدار موجود</label>
                                            <div class="form-group col-md-6 ">

                                                <input type="hidden" class="form-control" name="pritem"
                                                       value="{{$product->id}}">
                                                <select class="form-control" name="attr_item">
                                                    {{deleteItem($product, $attr_items, 'attributeitems', 'name')}}
                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <input type="submit" value="حذف شود" class="btn btn-danger">
                                            </div>
                                        </div>
                                    </form>
                                    <br>
                                    <form method="post" action="/admin/product/show/add">
                                        @csrf
                                        <div class="row">
                                            <label class="col-md-4 control-label"> مقدار اضافه</label>
                                            <div class="form-group col-md-6 ">

                                                <input type="hidden" class="form-control" name="pritem"
                                                       value="{{$product->id}}">
                                                <select class="form-control" name="item">
                                                    @php
                                                        fetchSort($attr_items,'name', 'id')
                                                    @endphp

                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <input type="submit" value="اضافه شود" class="btn btn-success">
                                            </div>
                                        </div>
                                    </form>
                                    <hr>
                                    <form method="post" action="/admin/product/del">
                                        @csrf
                                        <div class="row">
                                            <label class="col-md-4 control-label"> برند/مارک موجود</label>
                                            <div class="form-group col-md-6 ">

                                                <input type="hidden" class="form-control" name="prbrand"
                                                       value="{{$product->id}}">
                                                <select class="form-control" name="brand">
                                                    {{deleteItem($product, $brands, 'brands', 'name')}}
                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <input type="submit" value="حذف شود" class="btn btn-danger">
                                            </div>
                                        </div>
                                    </form>
                                    <br>
                                    <form method="post" action="/admin/product/show/add">
                                        @csrf
                                        <div class="row">
                                            <label class="col-md-4 control-label"> برند/مارک دیگر</label>
                                            <div class="form-group col-md-6 ">

                                                <input type="hidden" class="form-control" name="prbrand"
                                                       value="{{$product->id}}">
                                                <select class="form-control" name="brand">
                                                    @php
                                                        fetchSort($brands,'name', 'id')
                                                    @endphp

                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <input type="submit" value="اضافه شود" class="btn btn-success">
                                            </div>
                                        </div>
                                    </form>
                                    <hr>
                                    <form method="post" action="/admin/product/del">
                                        @csrf
                                        <div class="row">
                                            <label class="col-md-4 control-label"> تخفیف موجود</label>
                                            <div class="form-group col-md-6">

                                                <input type="hidden" class="form-control" name="prdis"
                                                       value="{{$product->id}}">
                                                <select class="form-control" name="discount">
                                                    {{deleteItem($product, $discounts, 'discounts', 'value')}}
                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <input type="submit" value="حذف شود" class="btn btn-danger">
                                            </div>
                                        </div>
                                    </form>
                                    <br>
                                    <form method="post" action="/admin/product/show/add">
                                        @csrf
                                        <div class="row">
                                            <label class="col-md-4 control-label"> تخفیف جدید</label>
                                            <div class="form-group col-md-6">

                                                <input type="hidden" class="form-control" name="prdis"
                                                       value="{{$product->id}}">
                                                <select class="form-control" name="discount">
                                                    @php
                                                        fetchSort($discounts,'value', 'id')
                                                    @endphp
                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <input type="submit" value="اضافه شود" class="btn btn-success">
                                            </div>

                                        </div>
                                    </form>
                                    <hr>
                                    <div class="row">
                                        {!! $product->getImagesEdit($product->id) !!}
                                        @csrf()

                                    </div>
                                </div>


                            </div>

                        </div>

                    </div>


                </div>
        </td>


        <td class="table">
            <input type="submit" class="btn btn-danger" value="حذف" @click="deletepro({{$product->id}})">
            @csrf()
        </td>
        </tr>
        @endforeach
        </tbody>
        </table>

    </div>
    <div class="card-footer text-center ">
        <h6></h6>
    </div>
</div>
</div>
