<div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-8">
        <form class="form cf">
            <div class="wizard left text-right">
                <div class="wizard-inner">
                    <div class="connecting-line"></div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="nav-item">
                            <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="مرحله اول"
                               class="nav-link active text-right ">
                                <span class="round-tab">
                                    <i class="fa fa-user"></i>
                                </span>
                            </a>
                            <p class="step-note">ورود به مزون نپال </p>
                        </li>
                        <li role="presentation" class="nav-item">
                            <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="مرحله نهایی"
                               class="nav-link next-step ">
                                <span class="round-tab next-step">
                                     <i class="fa fa-user next-step"></i><i class="fa fa-check"></i>
                                </span>
                            </a>
                            <p class="step-note">تکمیل حساب کاربری </p>
                        </li>
                    </ul>
                </div>
            </div>
        </form>
    </div>
    <div class="col-sm-2"></div>
</div>
<div class="wizard">
    <div class="tab-content left text-right">
        <div class="tab-pane active" role="tabpanel" id="step1">
            <div class="row">
                <div class="col-sm-6">
                    @include('partials.login')
                </div>
                <div class="col-sm-5 float-right">
                    <ul class="text-decoration-none p-2 mb-5 text-center">
                        <li class="list-group-item list-group-item-success">                هر چی که بخواهید! پشت درب تحویل بگیرید
                        </li>
                        <li class="list-group-item list-group-item-info">                چشمانی زیبا برای لباس هایی زیبا
                        </li>
                        <li class="list-group-item list-group-item-warning">                راحتی و آرامش در عین حال قیمت پایین
                        </li>
                        <li class="list-group-item list-group-item-danger">                طراحی شده برای شما
                        </li>
                        <li class="list-group-item list-group-item-primary">                شما نیاز دارید! ما داریمش
                        </li>
                        <li class="list-group-item list-group-item-dark">                کیفیتی که انتظار داشتید، قیمتی که انتظار نداشتید
                        </Li>
                    </ul>

                </div>
            </div>
        </div>
        <div class="tab-pane" role="tabpanel" id="step2">
            <h3 class="text-md-center text-secondary mb-5">در ابتدا ثبت نام کرده یا وارد پروفایل خود شوید</h3>

        </div>
        <div class="clearfix"></div>
    </div>
</div>