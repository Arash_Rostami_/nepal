<div class="col-md-8 main mx-auto d-block">
    <div class="card ">
        <div class="card-header text-center">
            <h3>مشاهده تخفیف ها</h3>
        </div>
        <div class="card-body text-center">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>نام تخفیف</th>
                    <th>میزان تخفیف</th>
                    <th>کد تخفیف</th>
                    <th>تاریخ شروع تخفیفد</th>
                    <th>تاریخ پایان تخفیف</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($discounts as $discount)
                    <tr>
                        <td class="table">{{$discount->name}}</td>
                        <td class="table">{{$discount->value}}%</td>
                        <td class="table">{{$discount->code}}</td>
                        <td class="table">{{$discount->begindate}}</td>
                        <td class="table">{{$discount->enddate}}</td>
                        <td class="table">
                            <input type="submit" class="btn btn-danger"
                                   value="حذف" @click="deletediscount({{$discount->id}})">
                            @csrf()
                        </td>
                    </tr>
                @endforeach

                </tbody>

            </table>

        </div>
        <div class="card-footer text-center ">
           <h6>{{ $discounts->render() }}</h6>
        </div>
    </div>
</div>