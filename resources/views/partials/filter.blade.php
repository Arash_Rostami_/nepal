<div class="row" id="tag_container" style="min-width: 100%; margin: 0; padding: 0 !important;">
    <div class="col-md-8 contents">

        <div class="row ">
            <div class="title-filter col-md-12 ">
                <h3 class="text-right"> {{$filters->name}}</h3>
            </div>
        </div>

        <div class="row left">
            <div class="col-sm-2 mt-3">
                <p class="sort-title text-right">
                    مرتب سازی بر اساس:
                </p>
            </div>

            <div class=" col-sm-2 mt-2 pr-0 mr-0">
                <select class="form-control right" id="sorting">
                    <option value="">انتخاب کنید</option>
                    <option value="price-Asc">قیمت کم به زیاد</option>
                    <option value="price-Des">قیمت زیاد به کم</option>
                </select>
            </div>

            <div class="col-sm-7 mt-2 ml-3 text-left clearfix">

                <span class="float-left">
                       @if (count($products) > 0)
                        <section class="pagination">
                           {{$products->links()}}
                        </section>
                    @endif
                </span>

            </div>
        </div>

        <div class="row left">

            <div class="col-sm-12 mt-3 grid">

                @foreach(array_chunk($products->all(), 3) as $productSet)
                    <div class="row">

                        @foreach ($productSet as $product)
                            <div id="pro{{$product->id}}" onmouseover="showSize({{$product->id}})"
                                 onmouseout="hideSize({{$product->id}})"
                                 class="col-xs-12 col-sm-12 col-lg-3 border shadow rounded-lg m-3 w-auto result-block"
                                 data-brand="{{showBrands($product)}}"
                                 data-attribute="{{showAttrs($product)}}"
                                 data-price="{{$product->price}}"
                            >
                                <div class="row">
                                    {{checkDiscountFilter($discounts, $product)}}
                                    {{checkProductFilter($product)}}
                                    {{checkHover($product)}}
                                    <div class="col-sm-12 text-right">
                                        <a href="#" data-toggle="modal"
                                           data-target="#myModal{{$product->id}}"
                                           onclick="showSlider({{$product->id}})">
                                            <img src="/images/main/mag.jpg" width="40px"
                                                 onclick="showSlider({{$product->id}})">
                                        </a>
                                        <!-- Modal -->
                                        <div class="modal fade" id="myModal{{$product->id}}" role="dialog">
                                            <div class="modal-dialog modal-lg overflow-auto" role="document"
                                                 onmouseenter="showSlider({{$product->id}})">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header bg-info">
                                                        <button type="button" class="close-pro-modal"
                                                                data-dismiss="modal">
                                                            &times;
                                                        </button>
                                                    </div>
                                                    <div class="modal-body modal-slide">
                                                        <div class="row">
                                                            {{checkDiscountModal($discounts, $product)}}
                                                            <a id="prev2" class="prev-pre"><i
                                                                        class="fa fa-chevron-up"></i> </a>
                                                            <a id="next2" class="next-pre"> <i
                                                                        class="fa fa-chevron-down"></i></a>
                                                            <div id="mycarousel{{$product->id}}">
                                                                @foreach($product->images as $image)
                                                                    <img src="{{$image->imagepath}}"
                                                                         width="100px"
                                                                         class="mt-4 mr-4 select">
                                                                @endforeach
                                                            </div>

                                                            <div class='col-sm-auto'
                                                                 id="extension{{$product->id}}"></div>
                                                            <div class="brand-logo">
                                                                <div class="col-sm-2">
                                                                    @foreach($product->brands as $brand)
                                                                        <img src="{{$brand->imagepath}}"
                                                                             width="100px"
                                                                             class="">
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="pro-name">
                                                            @foreach($product->brands as $brand)
                                                                <h1>{{$brand->name}}</h1>
                                                            @endforeach

                                                            <p>  {{$product->name}} </p>
                                                            <div class="d-inline-flex">
                                                                <h4 class='d-inline' style="color:#43C8D8">
                                                                    سایز:</h4>
                                                                <div class="p-2 mr-2">
                                                                    <select class="form-control right niceSelectJ proSize"
                                                                            onchange="enableCartModel()">
                                                                        <option data-display="گزینش کنید "></option>
                                                                        @foreach($product->sizes as $size)
                                                                            <option data-display="{{$size->size}}"
                                                                                    value="{{$size->id}}">
                                                                                {{$size->size}}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            {{writeDiscount($product)}}
                                                            <br>
                                                            <hr>
                                                            @if(Auth::guest())
                                                                <a role="button"
                                                                   class="purchase-button shadow btn btn-lg purchase-button-model"
                                                                   href="/logout"
                                                                   title="در ابتدا وارد حساب کاربری شوید">
                                                                    افزودن به سبد
                                                                    خرید <br>
                                                                    <i class="fa fa-cart-plus"></i>
                                                                </a>
                                                            @elseif(Auth::user())
                                                                <button class="purchase-button shadow btn btn-lg purchase-button-model"
                                                                        data-toggle="modal" data-target="#myCart" style="cursor: no-drop;"
                                                                        title="در ابتدا سایز مورد دلخواه کالا را گزینش نمایید"
                                                                        @click="addToCart({{$product->id}})" disabled>
                                                                    افزودن به سبد
                                                                    خرید <br>
                                                                    <i class="fa fa-cart-plus"></i>
                                                                </button>
                                                            @endif
                                                                <br>
                                                            <img src="/images/main/icon-pro.jpg"
                                                                 class="icon-pro mt-4">
                                                            <a href="/product/{{$product->id}}"
                                                               class="details-button shadow btn btn-success">
                                                                مشاهده جزییات محصول
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                            <br>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @include('partials.cart')
                                    </div>
                                </div>
                                <hr>
                                <h5 class="text-center bg-light p-3"> {{$product->name}}</h5>
                                <h6 class="pro-desc-fil text-center"
                                    title="{{$product->description}}"> {{$product->description}}</h6>
                                {{calDiscount($discounts, $product)}}
                                <div id="sizeList{{$product->id}}" class="text-center text-dark">
                                    {{showSizesAvail($product) }}
                                </div>
                            </div>

                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row my-3">
            <div class="col-md-2"></div>
            <div class="col-md-8 cat-menu text-right left h-auto accordion">
                <div class="card">
                    <div class="card-header collapsed">
                        <a class="card-link" data-toggle="collapse" href="#collapseOne">
                            انتخاب بر اساس: برند
                        </a>
                    </div>
                    <div id="collapseOne" class="collapse show" data-parent="#accordion">
                        <div class="card-body">
                            @foreach($brands as $brand)
                                <div class="filterBlock">
                                    <input type="checkbox" class="form-check-input brandFilter"
                                           value="{{$brand->name}}">
                                    <label class="form-check-label text-right mr-3">{{$brand->name}}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @foreach($attrs as $attr)
            <div class="row my-3 bor">
                <div class="col-md-2"></div>
                <div class="col-md-8 cat-menu text-right accordion">
                    <div class="card">
                        <div class="card-header collapsed">
                            <a class="card-link" data-toggle="collapse" href="#collapse{{$attr->id}}">
                                انتخاب بر اساس: {{$attr->name}}
                            </a>
                        </div>
                        <div id="collapse{{$attr->id}}" class="collapse show" data-parent="#accordion">
                            <div class="card-body attrBlock">
                                {{matchAttrs($attr->id, $attrItems)}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        <div class="row my-3">
            <div class="col-md-2"></div>
            <div class="col-md-8 cat-menu text-right accordion">
                <div class="card">
                    <div class="card-header collapsed">
                        <a class="card-link" data-toggle="collapse" href="#collapseThree">
                            انتخاب بر اساس: قیمت
                        </a>
                    </div>
                    <div id="collapseThree" class="collapse show w-auto" data-parent="#accordion">
                        <div class="card-body">
                            <input type="text" id="js-range-slider" name="my_range" value=""
                            />
                            <br>
                            <div class="row">
                                <div class="col-sm-12">
                                    <input width="10px" type="number" class="form-control text-center" value=""
                                           id="min-num"
                                           placeholder="حداقل"
                                           onchange="changeSlider()"/>
                                </div>
                                <br>
                                <div class="col-sm-12 pt-2">
                                    <input type="number" class="form-control text-center" value="" id="max-num"
                                           placeholder="حداکثر"
                                           onchange="changeSlider()"/>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        let iso = new Isotope('.grid', {
            itemSelector: '.result-block',
            layoutMode: 'fitRows',
            getSortData: {
                price: '.price',
            }
        });


    })

</script>




