<div class="flex-container row container-fluid banner-container">
    @foreach($banners as $banner)
        <div class="card col-sm-3 my-2 text-center img-fluid banner" style="border: none;">
            <img class="card-img-top img-fluid" src="{{$banner->image}}"
                 alt="Card image">
            <div class="card-body text-center img-fluid " style="background-color: #e3e3e3">
                {{$banner->text}}
            </div>
        </div>
    @endforeach
</div>

