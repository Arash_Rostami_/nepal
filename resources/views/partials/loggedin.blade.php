<template slot="auth">
    <div class="first-data left">
        <div class="dropdown">
            <button class="btn btn-dark dropdown-toggle left user-profile text-center"
                    type="button" data-toggle="dropdown">سلام {{Auth::user()->name}}
                <span class="caret"></span></button>
            <ul class="dropdown-menu user-profile text-center">
                <li><a href="/user/profile/{{Auth::id()}}">حساب کاربری</a></li>
                <li><a href="/user/order/{{Auth::id()}}">سفارشات</a></li>
                <li  onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><a href="{{ route('logout') }}"
                      >
                        {{ __('messages.Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: block;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
    </div>
</template>