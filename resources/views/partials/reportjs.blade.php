<script>

    //fetching data
    $.getJSON("/admin/orders/report", function (result) {
        let dateOrder = [];
        let dataOrder = [];
        for (let i = 0; i < result.length; i++) {
            dateOrder.push(result[i].date);
            dataOrder.push(result[i].total);
        }
        //storing data
        let myConfig = {
            type: 'bar',
            title: {
                text: 'آمار خرید بر حسب روز',
                fontSize: 20,
                fontFamily: 'BYekan',
            },
            scaleX: {
                label: {text: 'مورخه'},
                labels: dateOrder,
            },
            plot: {
                animation: {
                    effect: 'ANIMATION_EXPAND_BOTTOM',
                    method: 'ANIMATION_STRONG_EASE_OUT',
                    sequence: 'ANIMATION_BY_NODE',
                    speed: 275,
                },
            },
            series: [
                {
                    values: dataOrder,
                    text: 'تعداد خرید',
                    'background-color': '#3490DC',
                },
            ]
        };
        // Rendering data
        zingchart.render({
            id: 'chartDiv',
            data: myConfig,
            height: 300,
            width: 1000
        });
    });


    // updating action


    function changeAction(id) {
        let AjaxURL = '/admin/orders/update';
        let pro = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: AjaxURL,
            data: {
                id: pro,
            },
            success: function (result) {
                location.reload();
                swal({
                    buttons: false,
                    title: "باموفقیت",
                    text: "ارتقا یافت",
                    icon: 'success',
                    timer: 3000
                });
            }
        });
    }
</script>