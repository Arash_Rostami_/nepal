<a href="{{ route('logout') }}" class="btn btn-sm  btn-dark float-right mt-1" role="button"
   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
    <i class="fas fa-sign-out-alt" style="-webkit-transform: scaleX(-1);transform: scaleX(-1);"></i>
</a>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: block;">
    @csrf
</form>

