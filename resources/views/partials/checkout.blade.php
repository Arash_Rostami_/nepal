<div class="row left">
    <div class="col-sm-2"></div>
    <div class="col-sm-8 mx-auto d-block">
        <form class="form cf">
            <div class="wizard left text-right">
                <div class="wizard-inner">
                    <div class="connecting-line-credit"></div>
                    <div class="connecting-line-credit-green"></div>
                    <div class="connecting-line-credit-completion"></div>
                    <ul class="nav nav-tabs container" role="tablist">
                        <li role="presentation" class="nav-item">
                            <a data-target="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="مرحله اول"
                               class="nav-link active text-right ">
                                <span class="round-tab-credit" id="stage1">
                                    <i class="fas fa-shopping-cart"></i>
                                </span>
                            </a>
                            <p class="step-note-credit">سبد خرید </p>
                        </li>
                        <li role="presentation" class="nav-item">
                            <a data-target="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="مرحله دوم"
                               class="nav-link next-step ">
                                <span class="round-tab-credit next-step" id="stage2">
                                     <i class="fa fa-truck next-step" style="font-size: larger"></i>
                                </span>
                            </a>
                            <p class="step-note-credit">اطلاعات ارسال </p>
                        </li>
                        <li role="presentation" class="nav-item">
                            <a data-target="#step3" data-toggle="tab" aria-controls="step3" role="tab"
                               title="مرحله نهایی"
                               class="nav-link next-step ">
                                <span class="round-tab-credit next-step">
                                     <i class="fa fa-credit-card next-step"></i>
                                </span>
                            </a>
                            <p class="step-note-credit">اطلاعات پرداخت </p>
                        </li>
                    </ul>
                </div>
            </div>
        </form>
    </div>
    <div class="col-sm-2"></div>
</div>
<div class="wizard">
    <div class="tab-content left text-right">
        <div class="tab-pane active" role="tabpanel" id="step1">
            @include('partials.payment1')
        </div>
        <div class="tab-pane" role="tabpanel" id="step2">
            @includeif('partials.payment2')
        </div>
        <div class="tab-pane" role="tabpanel" id="step3">
            @includeif('partials.payment3')
        </div>
    </div>
    <div class="clearfix"></div>
</div>
</div>