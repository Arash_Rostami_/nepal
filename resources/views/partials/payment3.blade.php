<h4 class="p-2 mr-5 pr-5">خلاصه سفارش </h4>
<div class="row summary" onscroll="scroll()">

    <div class="col-sm-3"></div>
    <div class="col-sm-6 text-center">
        @if(!empty(Session::get('pro')))
            @foreach(array_unique(Session::get('pro')) as $key=>$value)
                <img src=" {{getProductImage($value)}}" width="175px" align="middle"
                     data-toggle="tooltip" data-placement="top"
                     title="{{getProAttr($value)}}">
            @endforeach
        @endif

    </div>
    <div class="col-sm-3"></div>
</div>
<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
        <table class="table table-price-final">
            <tbody>
            <tr>
                <td class="text-dark">مبلغ کل خرید</td>
                <td class="text-left text-dark">  {{number_format(Session::get('shopCart.total'))}} تومان</td>
            </tr>
            <tr>
                <td class="text-dark">هزینه ارسال، بیمه و بسته بندی</td>
                <td class="text-left text-dark">0 تومان</td>
            </tr>
            <tr>
                <td class="text-light">قابل پرداخت</td>
                <td class="text-left text-light"> {{number_format(Session::get('shopCart.total'))}} تومان</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-sm-3"></div>
</div>
<br><br>
<div class="row p-2">
    <div class="col-sm-3"></div>
    <div class="col-sm-1 text-right p-2">
        <h5 class="d-inline">کد تخفیف:</h5>
    </div>
    <div class="col-sm-2 p-2">
        <input class="form-control d-inline text-right" type="text" align="middle">
    </div>
    <div class="col-sm-1 text-right p-2">
        <h5 class="d-inline">کارت هدیه:</h5>
    </div>
    <div class="col-sm-2 p-2">
        <input class="form-control d-inline" type="text" align="middle">
    </div>
    <div class="col-sm-3"></div>
</div>
<br>
<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6 p-2">
        <i class="fas fa-truck d-inline"></i>
        @foreach($addresses as $address)
            @if(checkStatus($address))
                <p class=" d-inline">این سفارش به {{$address->name}} به آدرس {{$address->address}} با
                    شماره {{$address->mobile}} تحویل می گردد. </p>
            @endif
        @endforeach
    </div>
    <div class="col-sm-3"></div>
</div>
<br>
<div class="row text-center my-5">
    <div class="col-12 factor">
      <span class="px-5">
      <h5 class="d-inline">آیا مایل هستید بهمراه این سفارش فاکتور ارسال شود؟ </h5>
       <label class="d-inline" for="male">&nbsp;بلی </label>
          <input class="d-inline" type="radio" id="yes" name="mail" value="yes">
        <label class="d-inline" for="female">&nbsp;خیر</label>
           <input class="d-inline" type="radio" id="no" name="mail" value="no">
       </span>
    </div>
</div>
<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6 p-2">
        <h5 class=" d-flex mx-auto text-center">شیوه پرداخت: </h5>
    </div>
    <div class="col-sm-3"></div>
</div>
<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
        <table class="table border text-center">
            <tbody>
            <tr>
                <td class="table-address-radio-card text-center">
                    <input type="radio" name="payment" class="mt-4" onclick="enableFinal()">
                </td>
                <td>
                    <p> پرداخت اینترنتی (با کلیه کارتهای عضو شتاب) </p>
                    <img src="/images/main/bankslogo.jpg" width="250px">
                </td>
            </tr>
            <tr>
                <td class="table-address-radio-card text-center">
                    <input type="radio" class="mt-4" name="payment" id="real" onclick="enableFinalReal()" value="">
                </td>
                <td>
                    <p> پرداخت در محل: در حال حاضر مشتریانی که سفارش آنها از طریق ارسال اکسپری مزون نپال ارسال می شود،
                        توانسته کالای خود را پس از دریافت بصورت نقدی یا با کارت بانکی خود پرداخت نمایند </p>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-sm-3"></div>
</div>
<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6 p-2">
        <h5 class=" d-flex mx-auto text-center"> با انتخاب دکمه پرداخت و تکمیل خرید موافقت خود را با شرایط و قوانین نپال
            مزون اعلام می دارم </h5>
    </div>
    <div class="col-sm-3"></div>
</div>
<br>
<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6 p-2 text-center">

        @foreach($addresses as $address)
            @if(checkStatus($address))
                <button class="btn btn-lg btn-primary next-cart3" @click.prevent="zarinpal({{$address->id}})"
                        href="#final" title="درابتدا نحوه پرداخت را انتخاب کنید" disabled>پرداخت و تکمیل خرید
                </button>
            @endif
        @endforeach

    </div>
    <div class="col-sm-3"></div>
</div>