@extends('admin.profile')

@section('profile')



    <div class="col-sm-8">
        <div class="row my-3 ml-5">
            <div class="col-md-12 cat-menu text-right h-auto">
                <div id="accordionTwo">
                    <div class="card ">
                        <div class="card-header profile-card-menu">
                            <a class="card-link text-light" data-toggle="collapse" href="#collapseTwo">
                                <h3>مشاهده لیست خرید</h3>
                            </a>
                        </div>
                        <div id="collapseTwo" class="collapse show" data-parent="#accordionTwo">
                            <div class="card-body bg-light">
                                <div class="row">
                                    <table class="table shopping-table">
                                        <thead>
                                        <tr>
                                            <th>مشخصات محصول</th>
                                            <th>تعداد</th>
                                            <th>قیمت</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <@php
                                            foreach($lists as $list){
                                                $pro = array($list->product_id);
                                                $size = array($list->size_id);
                                                array_push($pro,$list->product_id);
                                                array_push($size,$list->size_id);
                                            }

                                        @endphp
                                        {{--@if(!empty(Session::get('pro')))--}}
                                        {{--@foreach(array_unique(Session::get('pro')) as $key=>$value)--}}
                                        @foreach($lists->unique('product_id') as $item)

                                            <td>


                                                <br>
                                                {{getProductName($item->product_id)}}
                                                <br>
                                                <img src="{{getProductImage($item->product_id)}}" width="120px">
                                                <br>
                                                <p>کد کالا: P{{$item->product_id}}</p>
                                            </td>
                                            <td>
                                                <div class="item-counter p-5">
                                                    <h5 class="text-dark bg-warning">{{countNumberOfPro($lists, $item)}}</h5>
                                                    <br><br>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-sm-4 pt-5">
                                                        <p class="d-inline pt-5">قیمت واحد کالا:</p>
                                                    </div>
                                                    <div class="col-sm-6 pt-5">
                                                        <p class="d-inline pt-5 mr-5">{{number_format(getProductPrice($item->product_id))}}
                                                            تومان</p>
                                                    </div>
                                                    <div class="col-sm-2 closing-cross align-middle text-center">
                                                        <i class="fa fa-remove pt-5 mt-5 text-secondary"
                                                           @click="removeFromList({{$item->product_id}}, {{Auth::id()}})"></i>
                                                    </div>
                                                </div>
                                                <hr class="p-0 m-0">
                                                <div class="row pt-0 mt-0 pb-3">
                                                    <div class="col-sm-4">
                                                        <p class="d-inline">قیمت کل:</p>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <p class="d-inline pt-5 mr-5">
                                                            {{  number_format(totalPriceList($lists, $item))}}
                                                            تومان</p>
                                                    </div>
                                                    <div class="col-sm-2 closing-cross align-middle text-center">
                                                    </div>
                                                </div>
                                            </td>
                                            </tr>
                                        @endforeach
                                        {{--@endif--}}
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row text-center">
                        <a role="button" href="/checkout/final/{{Auth::id()}}"
                           class="btn btn-lg btn-primary text-light
                            mx-auto">اتمام خرید</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>

@stop
