@extends('admin.layout')

@section('content')


    <brndcreate
            countrylist="{{ json_encode($countries) }}"
            parents="{{ $parents }}"
    >
        @include('partials.dropzonebrand')
    </brndcreate>


@stop