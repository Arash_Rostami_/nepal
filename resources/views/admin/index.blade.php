@extends('admin.layout')

@section('content')

    <div>
        <statspanel>
            <template slot="usersMax">{{$user}}</template>
            <template slot="commentsMax">{{$comment}}</template>
            <template slot="ordersMax">{{$order}}</template>
            <template slot="productsMax">{{$pro}}</template>
        </statspanel>


        <commenpanel>
            @include('partials.commentshow')
        </commenpanel>


        @include('partials.reports')


        <ordrpanel>
            @include('partials.ordershow')
        </ordrpanel>
    </div>


@stop