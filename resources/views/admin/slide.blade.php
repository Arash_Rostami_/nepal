@extends('admin.layout')

@section('content')

    <slidshow
            mainbrands="{{$brands}}"
            mainparents="{{$parents}}"
            maincats="{{$cats}}">
        @include('partials.dropzoneslide')
        <template slot="dropzone">
            @include('partials.dropzonebanner')
        </template>
    </slidshow>



@stop