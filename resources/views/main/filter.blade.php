<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no,
          initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Nepal Mason E-commercial Site">
    <meta name="keywords" content="Clothes, Dress, Uniform">
    <meta name="author" content="Arash Rostami">
    <meta name="theme-color" content="#2777A7">
    <title>Nepal Mason Shop</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ mix('css/app.css' )}}" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</head>
<body class="mainpage">

<div id="app">
    <headrmain>
        @if(!empty(Session::get('pro'))){{count(Session::get('pro'))}}@endif
        @if(Auth::user())
            @include('partials.loggedin')
        @elseif(!Auth::user())
            @include('partials.guest')
        @endif
    </headrmain>
    @include('partials.menu')
    @include('partials.filter')
    @include('main.signup')
    <contctmain></contctmain>
    @include('partials.footermain')

</div>

</body>
</html>
<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
@include('partials.scripts')
