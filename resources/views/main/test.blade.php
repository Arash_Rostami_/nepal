<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Nepal Mason E-commercial Site">
    <meta name="keywords" content="Clothes, Dress, Uniform">
    <meta name="author" content="Arash Rostami">
    <meta name="theme-color" content="#4285f4">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Error Page of NepalMason</title>

    <link href="{{ mix('css/app.css' )}}" rel="stylesheet">
</head>
<body>

<div id="app">


    <div class="row ">
        <div class="col-sm-1"></div>
        <div class="col-sm-10 text-center">
            <br><br><br><br>
            <img src="/images/main/error-page.jpg" width="500px" class="my-auto">
            <br><br><br>
            <a href="/index" class="btn btn-dark btn-lg" role="button"> بازگشت به صفحه اصلی</a>

        </div>
        <div class="col-sm-1"></div>
    </div>


</div>
<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>


</body>
</html>

