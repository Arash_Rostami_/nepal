<!-- Button trigger modal -->
{{--<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modelId">--}}
{{--Launch--}}
{{--</button>--}}

<!-- Modal -->
<div class="modal fade left " id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <button type="button" class="close-pro-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body col-sm-12 my-auto ">
                <div class="container-fluid text-center">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-justified">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#signTabOne">ورود به سایت</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#signTabTwo"> ثبت نام در سایت</a>
                        </li>

                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content text-right">
                        <div class="tab-pane container active" id="signTabOne">
                            @include('partials.login')
                        </div>
                        <div class="tab-pane container fade" id="signTabTwo">
                            @include('partials.register')
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
