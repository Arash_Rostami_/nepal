
<div class="row ">
    <div class="col-sm-1"></div>
    <div class="col-sm-10 text-center">
        <br><br><br><br>
        <img src="/images/main/error-page.jpg" width="500px" class="my-auto">
        <br><br><br>
        <a href="/index" class="btn btn-dark btn-lg" role="button">
            {{ $slot }}
        </a>
    </div>
    <div class="col-sm-1"></div>

</div>