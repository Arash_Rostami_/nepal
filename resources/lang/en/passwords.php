<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'گذر واژه شما پاک گردید',
    'sent' => 'ایمیلی برای تغییر گذر واژه ارسال گردید',
    'token' => 'کلید تغییر گذر واژه صحیح نمی باشد',
    'user' => "کاربری با چنین ایمیلی یافت نشده است",

];
