<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'E-Mail Address' => 'ایمیل',
    'Password' => 'گذرواژه',
    'Login' => 'ورود',
    'Logout' => 'خروج',
    'Remember Me' => 'ذخیره در حافظه',
    'Forgot Your Password?' => 'آیا گذرواژه را فراموش کردید؟',
    'Register' => 'ثبت نام',
    'Name' => 'نام کاربری',
    'Confirm Password' => 'تایید گذرواژه',
    'A fresh verification link has been sent to your email address.' => 'ایمیل جدیدی به آدرس ایمیل شمال ارسال گردید',
    'Before proceeding, please check your email for a verification link.' => 'قبل از اقدام دیگر، لطفا به ایمیل خود برای تایید رجوع نمایید',
    'If you did not receive the email' => 'در صورت عدم دریافت ایمیل',
    'click here to request another' => 'برای دریافت ایمیل دیگر اینجا کلیک نمایید',
    'LoginFill' => ' فرم زیر را پر نمایید',
    'Regards' => 'با تشکر',
    'Hello' => 'سلام',
    'Service' => 'سرویس اعلام پیام مشتری',
    'Reset Password' => 'تغییر گذرواژه ',
    'Send Password Reset Link' => 'ارسال ایمیل گذرواژه ',
    'Verify Your Email Address' => 'ایمیل ارسال شده را تایید نمایید',









];
