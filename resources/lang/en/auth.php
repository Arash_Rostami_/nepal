<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'failed' => 'این مشخصات با اطلاعات ثبت شده مطابقت ندارد',
    'throttle' => 'تعداد دفعه تلاش برای ورود بیش از حد مجاز بوده، دیرتر مجددا تلاش نمایید',

];
