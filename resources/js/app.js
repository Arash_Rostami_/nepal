/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./bs4');
require('./bs');


require('./sweetAlert');
require('./cycle');


require('./jquery.carouFredSel');
require('./jquery.nice-select');

require('./jQuerymin');
require('./slider');

require('./zingchart');
require('./dropzone');


window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

//admin panel
import headrpanel from './components/adminpanel/HeaderPanel';
import sidemenpanel from './components/adminpanel/SideMenuPanel';
import footrpanel from './components/adminpanel/FooterPanel';
import statspanel from './components/adminpanel/StatusPanel';
import commenpanel from './components/adminpanel/CommentPanel';
import ordrpanel from './components/adminpanel/OrderPanel';

//admin upload
import brndcreate from './components/adminupload/BrandCreate';
import categrycreate from './components/adminupload/CategoryCreate';
import discntcreate from './components/adminupload/DiscountCreate';
import prodctcreate from './components/adminupload/ProductCreate';
import slidshow from './components/adminupload/SlideCreate';
import usrcreate from './components/adminupload/UserCreate';
import usrshow from './components/adminupload/UserShow';

//main
import headrmain from './components/main/HeaderMain';
import slidmain from './components/main/SliderMain';
import promismain from './components/main/PromiseMain';
import logmain from './components/main/LogoMain';
import aboutmain from './components/main/AboutUsMain';
import contctmain from './components/main/ContactMain';
import promain from './components/main/ProductMain';


import VuePersianDatetimePicker from 'vue-persian-datetime-picker';

Vue.use(VuePersianDatetimePicker, {
    name: 'custom-date-picker',
    props: {
        inputFormat: 'YYYY-MM-DD',
        format: 'jYYYY-jMM-jDD',
        editable: false,
        inputClass: 'form-control my-custom-class-name',
        placeholder: 'گزینش نمایید',
        altFormat: 'YYYY-MM-DD',
        autoSubmit: false
    }
});


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import {myFunc} from './myFunc';

const app = new Vue({
    el: '#app',
    data: {
        address: [],
        edit: null
    },
    components: {
        headrpanel,
        sidemenpanel,
        footrpanel,
        statspanel,
        commenpanel,
        ordrpanel,
        brndcreate,
        categrycreate,
        discntcreate,
        prodctcreate,
        slidshow,
        usrcreate,
        usrshow,
        headrmain,
        slidmain,
        promismain,
        logmain,
        aboutmain,
        contctmain,
        promain,

    },
    methods: {
        changeStatus(id, status) {
            myFunc.changeStatus(id, status)
        },
        deleteComment(id) {
            myFunc.deleteCom(id)
        },
        deletebrand(id) {
            myFunc.deleteBrand(id);
        },
        deladdress(id) {
            myFunc.deleteAddress(id);
        },
        deletediscount(id) {
            myFunc.deleteDiscount(id);
        },
        deletepro(id) {
            myFunc.deletePro(id);
        },
        deleteimg(id) {
            myFunc.deleteImg(id);
        },
        commentResponse(id, user) {
            myFunc.showResponse(id, user);
        },
        changeStatusRes(id, status) {
            myFunc.changeStatusRes(id, status);
        },
        deleteCommentRes(id) {
            myFunc.deleteComRes(id);
        },
        addToCart(id) {
            myFunc.addToCart(id);
        },
        removeFromCart(id, value) {
            myFunc.deleteFromCart(id, value);
        },
        addToCartEdit(id) {
            myFunc.addToCartEdit(id);
        },
        removeFromCartEdit(id, value) {
            myFunc.removeFromCartEdit(id, value);
        },
        removeAllFromCart(id, value) {
            myFunc.removeAllFromCartEdit(id, value);
        },
        addstat() {
            myFunc.addstatus();
        },
        removeFromList(pro, user) {
            myFunc.removelist(pro, user);
        },
        userstat() {
            myFunc.changeUserStatus();
        },
        zarinpal(id) {
            myFunc.transaction(id);
        },
        editaddress(id) {
            var self = this;
            axios.get("/address/".concat(id)).then(function (response) {
                self.address = response.data;
                self.edit = true;
            });
        }

    }
});


require('jquery-validation');
require('./myJQ');
require('./lity');
