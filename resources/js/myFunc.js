export class myFunc {
    static deleteFromCart(id, value) {
        $.ajax({
            url: `/cartdelete/${id}`,
            headers: {
                'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content'),
            },
            method: 'post',
            data: {
                key: id,
                value: value,
            },
            success: function (data) {
                swal({
                    buttons: false,
                    title: "با موفقیت",
                    text: " از سبد خرید پاک شد ",
                    icon: 'success',
                    timer: 3000
                });
                setTimeout(function () {
                    location.reload()
                }, 1000)
            }
        });


    }

    static transaction(id) {
        let real = document.getElementById('real').value;
        let factor= $('.factor').find('input[type=radio]:checked').val();
        axios.post('/payment', {
            id: id,
            type: real,
            mail: factor,
        })
            .then(function () {
                (real == 'real') ? (window.location.href = '/verify') : '';
            }.bind(this))
            .catch(function (error) {
                alert(error);
            });
    }


    static addToCart(id) {
        let size= $(".proSize").val();
        axios.post(`/cart/${id}`, {
            product_id: id,
            size_id: size,
        })
            .then(function (response ) {
                console.log(response)
            })
            .catch(function(res) {
                if(res instanceof Error) {
                    console.log(res.message);
                } else {
                    console.log(res.data);
                }
            });
    }

    static addToCartEdit(id) {
        let size= $(".proSize"+id).text();
        $.ajax({
            url: `/cart/${id}`,
            headers: {
                'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content'),
            },
            method: 'get',
            data: {
                product_id: id,
                size_id: size
            },
            success: function (data) {
                console.log('success');
                swal({
                    buttons: false,
                    title: "با موفقیت",
                    text: " به سبد خرید اضافه شد ",
                    icon: 'success',
                    timer: 3000
                });
                setTimeout(function () {
                    location.reload()
                }, 1000)
            }
        });
        addOne(id);
    }

    static removeFromCartEdit(id, value) {
        $.ajax({
            url: `/cartdelete/${id}`,
            headers: {
                'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content'),
            },
            method: 'post',
            data: {
                key: id,
                value: value,
            },
            success: function (data) {
                swal({
                    buttons: false,
                    title: "با موفقیت",
                    text: " از سبد خرید پاک شد ",
                    icon: 'success',
                    timer: 3000
                });
                setTimeout(function () {
                    location.reload()
                }, 1000)
            }
        });
        minusOne(value);
    }


    static removelist(pro, user){
        swal({
            title: "آیا اطمینان دارید؟",
            text: "کل سبد شما خالی حواهد شد؟",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "خیر",
                    value: false,
                    visible: true,
                    closeModal: true,
                },
                confirm: {
                    text: "بلی",
                    value: true,
                    visible: true,
                    closeModal: true
                }
            }
        }).then(function (willDelete) {
            if (willDelete) {
                axios.post(`/user/cart/${pro}`, {
                    user:user,
                    pro: pro,
                }).then(function () {
                    location.reload(true);
                })
                    .catch(function (error) {
                        alert(error);
                    });
            } else {
                swal({
                    buttons: false,
                    title: "انصراف",
                    text: "کالا محفوظ میباشد",
                    icon: "error",
                    timer: 3000
                });
            }
        });


    }

    static removeAllFromCartEdit(id, value) {
        $.ajax({
            url: `/cartdeleteall/${id}`,
            headers: {
                'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content'),
            },
            method: 'post',
            data: {
                key: id,
                value: value,
            },
            success: function (data) {
                swal({
                    buttons: false,
                    title: "با موفقیت",
                    text: " از سبد خرید پاک شد ",
                    icon: 'success',
                    timer: 3000
                });
                setTimeout(function () {
                    location.reload()
                }, 1000)
            }
        });
    }

    static deleteComRes(id) {
        swal({
            title: "آیا اطمینان دارید؟",
            text: "کامنت/نظر پاک خواهد شد",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "خیر",
                    value: false,
                    visible: true,
                    closeModal: true,
                },
                confirm: {
                    text: "بلی",
                    value: true,
                    visible: true,
                    closeModal: true
                }
            }
        }).then(function (willDelete) {
            if (willDelete) {
                axios.delete(`/admin/comment/res/${id}`, {
                    id: id,
                }).then(function () {
                    swal({
                        buttons: false,
                        title: "پاک شد",
                        text: " ",
                        icon: "success",
                        timer: 3000
                    });
                    location.reload(true);
                })
                    .catch(function (error) {
                        alert(error);
                    });
            } else {
                swal({
                    buttons: false,
                    title: "انصراف",
                    text: "نظر محفوظ میباشد",
                    icon: "error",
                    timer: 3000
                });
            }
        });
    }

    static changeUserStatus(){
        let id = $('.user-address').find('input[type=radio]:checked').val();
        axios.post('ad', {
            id: id,
        })
            .then(function () {
                swal({
                    buttons: false,
                    title: "باموفقیت",
                    text: "آدرس ارسالی مرجع گزینش شد",
                    icon: "success",
                    timer: 3000
                });
                location.reload();

            }.bind(this))
            .catch(function (error) {
                alert(error);
            });
    }

    static addstatus() {
        let id = $('.table-address').find('input[type=radio]:checked').val();
        axios.post('/address/update', {
            id: id,
        })
            .then(function () {
                // $('#step3').tab('show');
                location.reload();

            }.bind(this))
            .catch(function (error) {
                alert(error);
            });
    }

    static changeStatusRes(id, status) {
        axios.post('./comment/res', {
            id: id,
            status: status
        })
            .then(function () {
                myFunc.changed('تغییر وضعیت', id);
            }.bind(this))
            .catch(function (error) {
                alert(error);
            });
    }

    static showResponse(id, user) {
        $('#bt' + id).hide('slow');
        let token = $('meta[name="csrf-token"]').attr('content');
        $(".row").find(`[data-comment='${id}']`).after(`
                    <div class="row">
                        <div class="col-sm-12">
                            <form action="/comment/response" method="post">
                            <input type="hidden" name="_token" id="csrf-token" value="${token}"/>                                <div class="form-group">
                                  <div class='text-right'>
                                   <label for="comment">نظر</label>
                                   </div>
                                  <input type="hidden" name="user_id" value="${user}">
                                   <input type="hidden" name="comment_id" value="${id}">
                                   <textarea class="form-control" name="comment" rows="5" required></textarea>
                                </div>
                                 <div class='col-sm-12 text-left'>
                                <button type="submit" class="btn btn-success mb-2">ارسال</button>
                                </div>
                         </div>
                    </div>
`);
    }

    static deleteCom(id) {
        swal({
            title: "آیا اطمینان دارید؟",
            text: "کامنت/نظر پاک خواهد شد",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "خیر",
                    value: false,
                    visible: true,
                    closeModal: true,
                },
                confirm: {
                    text: "بلی",
                    value: true,
                    visible: true,
                    closeModal: true
                }
            }
        }).then(function (willDelete) {
            if (willDelete) {
                axios.delete(`/admin/comment/${id}`, {
                    id: id,
                }).then(function () {
                    swal({
                        buttons: false,
                        title: "پاک شد",
                        text: " ",
                        icon: "success",
                        timer: 3000
                    });
                    location.reload(true);
                })
                    .catch(function (error) {
                        alert(error);
                    });
            } else {
                swal({
                    buttons: false,
                    title: "انصراف",
                    text: "نظر محفوظ میباشد",
                    icon: "error",
                    timer: 3000
                });
            }
        });
    }

    static changeStatus(id, status) {
        axios.post('./comment', {
            id: id,
            status: status
        })
            .then(function () {
                myFunc.changed('تغییر وضعیت', id);
            }.bind(this))
            .catch(function (error) {
                alert(error);
            });
    }


    static saving(btn) {
        btn.setAttribute('value', " در حال ذخیره  ");
        setTimeout(function () {
            btn.setAttribute('value', " در حال ذخیره .");
        }, 350);
        setTimeout(function () {
            btn.setAttribute('value', " در حال ذخیره ..");
        }, 600);
        setTimeout(function () {
            btn.setAttribute('value', " در حال ذخیره ...");
        }, 1000);
        setTimeout(function () {
            btn.setAttribute('value', " در حال ذخیره ....");
        }, 1500);
        setTimeout(function () {
            btn.setAttribute('value', "  ذخیره ");
        }, 2000);
    }

    static changed(message, id) {
        setTimeout(function () {
            swal({
                buttons: false,
                title: `${message}`,
                text: `${id} در کامنت شماره `,
                icon: "success",
                timer: 3000
            });
            setTimeout(function () {
                location.reload(true)
            }, 2000);
        }, 2000);
    }

    static flash(self) {
        setTimeout(function () {
            swal({
                buttons: false,
                title: "با موفقیت",
                text: `${self} ذخیره گردید`,
                icon: "success",
                timer: 3000
            });
            setTimeout(function () {
                location.reload(true)
            }, 3000);
            location.reload(true)
        }, 2500);
    }

    static wrong(self) {
        setTimeout(function () {
            swal({
                buttons: false,
                title: "خطا",
                text: `${self} `,
                icon: "error",
                timer: 3000
            });
        }, 1000);
    }

    static insertion(response) {
        document.getElementById("upname").value = response.data.name;
        document.getElementById("uplname").value = response.data.lname;
        document.getElementById("uprole").value = response.data.role;
        document.getElementById("upemail").value = response.data.email;
        document.getElementById("uppassword").value = response.data.password;
        document.getElementById("upid").value = response.data.id;
    }

    static flashwithnorefresh(self) {
        setTimeout(function () {
            swal({
                buttons: false,
                title: "با موفقیت",
                text: `${self} ذخیره گردید`,
                icon: "success",
                timer: 3000
            });

        }, 2500);
    }

    static pointer(self) {
        swal({
            buttons: false,
            title: "توجه",
            text: self,
            icon: "info",
            timer: 3000
        });
    }

    static deleteDiscount(id) {
        swal({
            title: "آیا اطمینان دارید؟",
            text: "تخفیف پاک خواهد شد",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "خیر",
                    value: false,
                    visible: true,
                    closeModal: true,
                },
                confirm: {
                    text: "بلی",
                    value: true,
                    visible: true,
                    closeModal: true
                }
            }
        }).then(function (willDelete) {
            if (willDelete) {
                axios.delete(`/admin/discount/${id}`, {
                    id: id,
                }).then(function () {
                    location.reload(true);
                })
                    .catch(function (error) {
                        alert(error);
                    });
            } else {
                swal({
                    buttons: false,
                    title: "انصراف",
                    text: "تخفیف محفوظ میباشد",
                    icon: "error",
                    timer: 3000
                });
            }
        });
    }

    static deleteGroup(id) {
        swal({
            title: "آیا اطمینان دارید؟",
            text: "دسته کالا پاک خواهد شد",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "خیر",
                    value: false,
                    visible: true,
                    closeModal: true,
                },
                confirm: {
                    text: "بلی",
                    value: true,
                    visible: true,
                    closeModal: true
                }
            }
        }).then(function (willDelete) {
            if (willDelete) {
                axios.delete(`/admin/product/gr/${id}`, {
                    id: id,
                }).then(function () {
                    swal({
                        buttons: false,
                        title: "با موفقیت",
                        text: ` حذف گردید`,
                        icon: "success",
                        timer: 3000
                    });
                    location.reload(true);
                })
                    .catch(function (error) {
                        alert(error);
                    });
            } else {
                swal({
                    buttons: false,
                    title: "انصراف",
                    text: "دسته محفوظ میباشد",
                    icon: "error",
                    timer: 3000
                });
            }
        });
    }

    static deleteAddress(id) {
        swal({
            title: "آیا اطمینان دارید؟",
            text: "آدرس پاک خواهد شد",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "خیر",
                    value: false,
                    visible: true,
                    closeModal: true,
                },
                confirm: {
                    text: "بلی",
                    value: true,
                    visible: true,
                    closeModal: true
                }
            }
        })
            .then((willDelete) => {
                if (willDelete) {
                    axios.delete(`/address/${id}`, {
                        id: id,
                    }).then(function () {
                        location.reload(true);
                    })
                        .catch(function (error) {
                            alert(error);
                        });
                } else {
                    swal({
                        buttons: false,
                        title: "انصراف",
                        text: "برند محفوظ میباشد",
                        icon: "error",
                        timer: 3000
                    });
                }
            });
    }

    static deleteBrand(id) {
        swal({
            title: "آیا اطمینان دارید؟",
            text: "برند پاک خواهد شد",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "خیر",
                    value: false,
                    visible: true,
                    closeModal: true,
                },
                confirm: {
                    text: "بلی",
                    value: true,
                    visible: true,
                    closeModal: true
                }
            }
        })
            .then((willDelete) => {
                if (willDelete) {
                    axios.delete(`./brand/${id}`, {
                        id: id,
                    }).then(function () {
                        location.reload(true);
                    })
                        .catch(function (error) {
                            alert(error);
                        });
                } else {
                    swal({
                        buttons: false,
                        title: "انصراف",
                        text: "برند محفوظ میباشد",
                        icon: "error",
                        timer: 3000
                    });
                }
            });
    }


    static deletePro(id) {
        swal({
            title: "آیا اطمینان دارید؟",
            text: "محصول پاک خواهد شد",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "خیر",
                    value: false,
                    visible: true,
                    closeModal: true,
                },
                confirm: {
                    text: "بلی",
                    value: true,
                    visible: true,
                    closeModal: true
                }
            }
        })
            .then((willDelete) => {
                if (willDelete) {
                    axios.delete(`/admin/product/${id}`, {
                        id: id,
                    }).then(function () {
                        location.reload(true);
                    })
                        .catch(function (error) {
                            alert(error);
                        });
                } else {
                    swal({
                        buttons: false,
                        title: "انصراف",
                        text: "محصول محفوظ میباشد",
                        icon: "error",
                        timer: 3000
                    });
                }
            });
    }


    static deleteImg(id) {
        swal({
            title: "آیا اطمینان دارید؟",
            text: "عکس پاک خواهد شد",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "خیر",
                    value: false,
                    visible: true,
                    closeModal: true,
                },
                confirm: {
                    text: "بلی",
                    value: true,
                    visible: true,
                    closeModal: true
                }
            }
        })
            .then((willDelete) => {
                if (willDelete) {
                    axios.delete(`/admin/product/img/${id}`, {
                        id: id,
                    }).then(function () {
                        swal({
                            buttons: false,
                            title: "با موفقیت",
                            text: ` حذف گردید`,
                            icon: "success",
                            timer: 3000
                        });
                        location.reload(true);
                    })
                        .catch(function (error) {
                            alert(error);
                        });
                } else {
                    swal({
                        buttons: false,
                        title: "انصراف",
                        text: "عکس محفوظ میباشد",
                        icon: "error",
                        timer: 3000
                    });
                }
            });
    }
}
