


$(document).ready(function () {

    $(".discount , .discountNum").delay(2000).fadeIn(500);
    $(".dis , .disnumer").delay(2000).fadeIn(500);


    $(".grey-brand").hover(function () {
        $(this).animate({"width": "130px"}, 500)
    }, function () {
        $(this).animate({"width": "100px"}, 750);
        $(this).off('mouseenter');
    });


    $('.niceSelectJ').niceSelect();


    $(window).resize(function () {
        $('.zoom').fadeOut();
    });

    $.getScript(
        'https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/js/ion.rangeSlider.min.js', function () {
            $("#js-range-slider").ionRangeSlider({
                type: "double",
                min: 0,
                max: 10000000,
                from: 30000,
                to: 6000000,
                grid: true,
                grid_num: 2,
                step: 10000,
                skin: "big",
                hide_min_max: true,
                prefix: 'Toman ',
                onFinish(data) {
                    let min = $('#min-num').val(data.from);
                    let max = $('#max-num').val(data.to);
                    $(".result-block").each(function () {
                        let item = $(this).attr('data-price');
                        if (item >= data.from && item <= data.to) {
                            $(this).fadeIn('slow');
                        } else {
                            $(this).hide('slow');
                        }
                    });
                }

            });
        });


    // user profile validation through JQuery Validator Plugin

    $("#submitButton").click(function () {
            $("#myForm").validate({
                rules: {
                    fname: "required",
                    lname: "required",
                    gender: "required",
                    mobile:"required",
                    state:{min:1},
                    city:{min:1},
                    address:"required",
                    dbirth:{
                        required: true,
                        min:1,
                        max:31
                    },
                    mbirth:{
                        required: true,
                        min:1,
                        max:12
                    },
                    ybirth: {
                        required: true,
                        minlength: 4,
                        min:1300,
                        max:2050,
                    }
                },
                messages: {
                    fname: "* نام را وارد نمایید",
                    lname: "* نام خانوادگی را کامل وارد نمایید",
                    gender: "  * جنسیت را انتخاب نمایید",
                    mobile:"* شماره همراه را وارد نمایید",
                    state:"* ",
                    city:"* ",
                    address:"* لطفا آدرس را به فارسی وارد نمایید ",
                    dbirth:{
                        required:"روز تولد را وارد نمایید",
                        min:"روز به اشتباه درج شده",
                        max:"روز به اشتباه درج شده",
                    },
                    mbirth:{
                        required:"ماه تولد را وارد نمایید",
                        min:"ماه به اشتباه درج شده",
                        max:"ماه به اشتباه درج شده",
                    },
                    ybirth: {
                        required:"سال تولد را وارد نمایید",
                        minlength: "چهار رقم سال را بطور کامل وارد نمایید",
                        min:"سال به اشتباه درج شده",
                        max:"سال به اشتباه درج شده",
                    },
                },
                errorClass: "invalid",
                validClass: "success",

                submitHandler: function(form) {
                   form.submit()
                }
            });
        });
});


