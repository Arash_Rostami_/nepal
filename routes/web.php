<?php


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/show', 'HomeController@show');
Route::get('/house', 'HomeController@house');

//google api
Route::get('login/google', 'Auth\LoginController@redirectToProvider');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');

//homepage links
Route::get('/index', 'HomePageController@show');
Route::post('/search', 'HomePageController@search');
Route::get('/index/data', 'HomePageController@email');
Route::get('/filter/{id}', 'HomePageController@filter');
Route::get('/product/{id}', 'HomePageController@product');

//comment pages
Route::post('/addcomment', 'CommentController@store');
Route::post('/comment/response', 'CommentController@reply');

//shopping cart pages
Route::any('/cart/{id}', 'CardController@add')->middleware('auth');
Route::post('/cartdelete/{id}', 'CardController@remove');
Route::post('/cartdeleteall/{id}', 'CardController@destroy');
Route::post('/payment', 'CardController@zarinpal');
Route::any('/verify', 'CardController@verify')->name('verify');
Route::get('/checkout/final/{id}', 'CardController@final')->middleware('auth');


//shopping cart login page
Route::get('/logout', 'HomePageController@checklogin');
Route::post('/address', 'HomePageController@add');
Route::delete('/address/{id}', 'HomePageController@delete');
Route::get('/address/{id}', 'HomePageController@index');
Route::post('/address/update', 'HomePageController@status');


//user profile page
Route::get('user/profile/{id}', 'UserController@profile')->middleware('auth');
Route::get('user/update/{id}', 'UserController@info')->middleware('auth');
Route::get('user/address/{id}', 'UserController@ad')->middleware('auth');
Route::get('user/order/{id}', 'UserController@order')->middleware('auth');
Route::get('user/comment/{id}', 'UserController@comment')->middleware('auth');
Route::get('user/list/{id}', 'UserController@list')->middleware('auth');
Route::get('user/question/{id}', 'UserController@question')->middleware('auth');

Route::post('user/question/submit', 'UserController@submit')->middleware('auth');
Route::post('user/address/ad', 'UserController@mainad')->middleware('auth');
Route::post('user/ind', 'UserController@ind')->middleware('auth');
Route::post('/user/cart/{id}', 'UserController@empty')->middleware('auth');


// error page
Route::get('error', 'HomePageController@error');


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    //index
    Route::get('index', 'AdminController@index');
    Route::resource('category', 'CategoryController');

    //product
    Route::resource('product', 'ProductController');
    Route::get('product/show', 'ProductController@show');
    Route::delete('product/gr/{id}', 'ProductController@delete');
    Route::patch('product/update', 'ProductController@update');
    Route::post('product/show/add', 'ProductController@add');
    Route::post('product/del', 'ProductController@del');

    Route::post('cat', 'GroupController@store');
    Route::post('group', 'GroupController@addAtrribute');
    Route::post('attr', 'GroupController@addAttributeitem');
    Route::post('size', 'GroupController@addSize');
    Route::post('image', 'ImageController@addImage');

    //brand
    Route::resource('brand', 'BrandController');

    //discount
    Route::get('discount', 'DiscountController@show')->name('discountShow');
    Route::get('discount/create', 'DiscountController@index')->name('discountCreate');
    Route::get('discount/products', 'DiscountController@products');
    Route::post('discount/store', 'DiscountController@store');
    Route::delete('discount/{id}', 'DiscountController@destroy');

    //user
    Route::get('user', 'UserController@show')->name('userShow');
    Route::get('user/create', 'UserController@index')->name('userCreate');
    Route::post('user/store', 'UserController@store');
    Route::post('user/status', 'UserController@status');
    Route::get('user/status', 'UserController@fetchstatus');
    Route::delete('user/{id}', 'UserController@destroy');
    Route::get('user/update/{id}', 'UserController@retrieve');
    Route::post('user/update', 'UserController@edit');

    //slide & image
    Route::delete('product/img/{id}', 'ImageController@destroy');
    Route::get('slide', 'ProductController@uploadImage');
    Route::post('slide/upload', 'ProductController@uploadSlide');
    Route::post('banner/upload', 'ProductController@uploadBanner');

    //comments
    Route::get('/comment', 'CommentController@show');
    Route::post('/comment', 'CommentController@status');
    Route::delete('/comment/{id}', 'CommentController@destroy');
    Route::post('/comment/res', 'CommentController@state');
    Route::delete('/comment/res/{id}', 'CommentController@delete');

    //Orders
    Route::get('/orders', 'OrderController@show');
    Route::post('/orders/update', 'OrderController@update');
    Route::get('/orders/report', 'OrderController@report');

});