<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    public $timestamps = false;
    protected $fillable = [
        'name', 'parent_id'
    ];

    public function order()
    {
        return $this->belongsTo('App/Order');
    }

    public function products()
    {
        return $this->hasManyThrough(Product::class, Group::class, 'category_id', 'group_id');
    }

    public function groups()
    {
        return $this->hasMany(Group::class);
    }

    public function parentCategory()
    {
        return $this->belongsTo(ParentCategory::class, 'parent_id');
    }


}
