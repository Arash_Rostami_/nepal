<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';
    public $timestamps = false;
    protected $fillable = [
        'name', 'country', 'imagepath', 'parent_id'
    ];
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function parents()
    {
        return $this->belongsTo(ParentCategory::class);

    }
}
