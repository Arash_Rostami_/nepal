<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentCategory extends Model
{
    protected $table = 'parents';
    public $timestamps = false;
    protected $fillable = [
        'name',
    ];

    public function categories()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }
    public function brands()
    {
        return $this->hasMany(Brand::class);

    }

    public function groups()
    {
        return $this->hasManyThrough(Group::class, Category::class, 'parent_id', 'category_id');
    }
}
