<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserQuery extends Notification
{
    use Queueable;

    public function __construct($question, $user, $email)
    {
        $this->question = $question;
        $this->user = $user;
        $this->email = $email;

    }


    public function via($notifiable)
    {
        return ['mail'];
    }


    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('سلام')
            ->subject('سوال کاربر ')
            ->line('  کاربر' .$this->user)
            ->line($this->email. '  با ایمیل ')
            ->line('  برای شما پیام زیر را ارسال نموده است')
            ->line(' ')
            ->line($this->question)
            ->line('');
    }


    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
