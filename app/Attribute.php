<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    public $timestamps = false;
    protected $table = 'attributes';
    protected $fillable = [
        'name', 'group_id'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function attributeItems()
    {
        return $this->hasMany(Attributeitem::class);

    }

    public function getAttributeItem($id)
    {
        $items = $this->find($id)->attributeItems;
        foreach ($items as $item) {
            echo $item->name;
        }
    }
}
