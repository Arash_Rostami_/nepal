<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingList extends Model
{
    protected $table = 'lists';
    protected $fillable = [
        'product_id' ,'size_id', 'user_id'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function size()
    {
        return $this->belongsTo(Size::class, 'size_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
