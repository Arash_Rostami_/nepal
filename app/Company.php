<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    protected $fillable = [
        'name' , 'code', 'idcode' ,'phone' ,'number' ,'province' ,'city' ,'user_id', 'address'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
