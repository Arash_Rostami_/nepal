<?php

use App\Comment;
use App\Order;
use App\User;
use Carbon\Carbon;
use function foo\func;

function flashMessage($title, $message, $type)
{
    $flashMessageClass = App('App\Http\Utilities\flashMessageClass');
    return $flashMessageClass->flashMessageClassFunc($title, $message, $type);
}


function countriesName()
{
    $countriesNameClass = App('App\Http\Utilities\Country');
    return $countriesNameClass::all();
}

function statesName()
{
    $statesNameClass = App('App\Http\Utilities\Country');
    return $statesNameClass::states();
}

function citiesName()
{
    $citiesNameClass = App('App\Http\Utilities\Country');
    return $citiesNameClass::cities();
}


function checkProduct($product)
{
    $checkProductClass = App('App\Http\Utilities\checkPro');
    return $checkProductClass->checkProduct($product);
}

function checkProductFilter($product)
{
    $checkProductClass = App('App\Http\Utilities\checkProFil');
    return $checkProductClass->checkProduct($product);
}

function checkHover($product)
{
    $checkProductClass = App('App\Http\Utilities\chechHover');
    return $checkProductClass->checkProduct($product);
}

function checkBanner($parent)
{
    $checkBannerClass = App('App\Http\Utilities\checkBanner');
    return $checkBannerClass->checkBan($parent);
}

function checkDiscount($ds, $pr)
{
    $checkDiscountClass = App('App\Http\Utilities\checkDiscount');
    return $checkDiscountClass->checkDisc($ds, $pr);
}

function checkDiscountFilter($ds, $pr)
{
    $checkDiscountClass = App('App\Http\Utilities\checkDiscountFilter');
    return $checkDiscountClass->checkDisc($ds, $pr);
}

function checkDiscountModal($ds, $pr)
{
    $checkDiscountClass = App('App\Http\Utilities\checkDiscountModal');
    return $checkDiscountClass->checkDisc($ds, $pr);
}

function discountCal($price, $discount)
{
    $subst = ($price * $discount) / 100;
    $sum = $price - $subst;
    return number_format($sum);
}

function discountedSum($product)
{
    foreach ($product->discounts as $discount) {
        $price = $product->price;
        $discount = $discount->value;
        $subst = ($price * $discount) / 100;
    }
    return number_format($subst);
}

function showTitle()
{
    echo "<script>(function hideTitle(){document.getElementById('others').classList.remove(\"d-none\");})() </script>";
}

function discountRate($product)
{
    foreach ($product->discounts as $discount) {
        $discount = $discount->value;
    }
    if ($discount) {
        return "%" . number_format($discount);
    }
}

function calDiscount($discounts, $product)
{
    $calculateDiscount = App('App\Http\Utilities\calDiscount');
    return $calculateDiscount->calDisc($discounts, $product);
}

function writeDiscount($product)
{
    $writeDiscount = App('App\Http\Utilities\writeDiscount');
    return $writeDiscount->writeDisc($product);
}

function thisMoment()
{
    return Morilog\Jalali\Jalalian::now();
}

function getTodayDate()
{
    $todayDate = App('App\Http\Utilities\dateGet');
    return $todayDate->getDate();
}

function fetchSort($targets, $attr, $val)
{
    $sorted = App('App\Http\Utilities\sortVariables');
    return $sorted->sortAll($targets, $attr, $val);
}

function deleteItem($product, $items, $relation, $show)
{
    $selected = App('App\Http\Utilities\deleteFile');
    return $selected->delFile($product, $items, $relation, $show);
}

function showSizesAvail($product)
{
    $sizes = App('App\Http\Utilities\showSizes');
    return $sizes->showSizes($product);
}

function showBrands($product)
{
    $brands = App('App\Http\Utilities\fetchBrand');
    return $brands->fetchBrand($product);
}

function showAttrs($product)
{
    $attrs = App('App\Http\Utilities\fetchAttrs');
    return $attrs->fecthAttrs($product);
}

function matchAttrs($id, $items)
{
    $matched = App('App\Http\Utilities\matchAttrItem');
    return $matched->matchAttrs($id, $items);
}

function fetchTable($product)
{
    $feat = App('App\Http\Utilities\featureTable');
    return $feat->fecthFeatures($product);
}

function showOtherProducts($groups, $product, $discounts)
{
    $pro = App('App\Http\Utilities\similarPros');
    return $pro->others($groups, $product, $discounts);
}

function showProductComments($product)
{
    $comments = App('App\Http\Utilities\showComments');
    return $comments->showComments($product);
}

function getProductName($product)
{
    $pro = App('App\Http\Utilities\getProduct');
    return $pro->getProName($product);
}

function getProductPrice($product)
{
    $pro = App('App\Http\Utilities\getProduct');
    return $pro->getProPrice($product);
}

function getProductImage($product)
{
    $pro = App('App\Http\Utilities\getProduct');
    return $pro->getProImage($product);
}

function getProductSize($size)
{
    $pro = App('App\Http\Utilities\getProduct');
    return $pro->getProSize($size);
}

function getProAttr($product)
{
    $pro = App('App\Http\Utilities\getProduct');
    return $pro->getProAttr($product);
}

function checkAddress($address)
{
    return $address->user_id == Auth::id();
}

function checkStatus($address)
{
    return $address->user_id == Auth::id() && $address->status == 1;
}

function countPro($order_id, $product_id)
{
    return DB::table('order_product')
        ->where('order_id', $order_id)->where('product_id', $product_id)->count();
}

function showCityByPro($order)
{
    foreach (citiesName() as $key => $value)
        return $key;

}

function showStateByPro($order)
{
    foreach (statesName() as $key => $value)
        if ($key == $order)
            return $value;
}

function showBrandByPro($pro)
{
    foreach ($pro as $brand)
        return $brand->name;
}

function showImageByPro($pro)
{
    foreach ($pro->images as $image)
        return $image->imagepath;

}

function maxOrder($id)
{
    return DB::table('orders')->where('user_id', $id)->count('id');
}

function maxOrderConfirmed($id)
{
    return DB::table('orders')->where('user_id', $id)->where('action', 1)->count('id');
}

function userAddress($id)
{
    return DB::table('addresses')->where('user_id', $id)->where('status', 1);
}

function countNumberOfPro($lists, $item)
{
    $price = App('App\Http\Utilities\countShopItem');
    return $price->countEachItem($lists, $item);
}

function totalPriceList($lists, $item)
{
    return getProductPrice($item->product_id) * countNumberOfPro($lists, $item);
}

function countNewUser()
{
    return User::where('created_at', '>', Carbon::today()->subDays(7))->count();
}

function countNewComment()
{
    return Comment::where('status', 0)->count();
}

function countNewPurchase()
{
    return Order::where('action', 0)->orWhereNull('action')->count();
}


