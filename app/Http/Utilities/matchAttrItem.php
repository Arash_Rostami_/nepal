<?php
/**
 * Created by PhpStorm.
 * User: Arash Rostami
 * Date: 2/5/2020
 * Time: 8:30 PM
 */

namespace App\Http\Utilities;


class matchAttrItem
{
    public function matchAttrs($id, $items)
    {
        foreach ($items as $item) {
            if ($item->attribute_id == $id) {
                echo " <input type='checkbox' class='form-check-input attrFilter' value='{$item->id}'>
                    <label class='form-check-label text-right mr-3'>   {$item->name} </label>
                    ";
            }
        }
    }
}