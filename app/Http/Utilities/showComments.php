<?php
/**
 * Created by PhpStorm.
 * User: Arash Rostami
 * Date: 2/19/2020
 * Time: 8:29 PM
 */

namespace App\Http\Utilities;


class showComments
{
    public function showComments($product)
    {
        $comments = $product->comments->where('status', 1)->sortByDesc('id')->all();
        $id = session('user');

        $commentBody = "";
        foreach ($comments as $comment) {

            $commentBody .= " 
             <div class='row bg-dark text-light' style='max-height: 35px'>
                    <div class='col-sm-6 text-right'>
                        <p class='p-2'><i class='ml-2 fas fa-user'></i> {$comment->user->name}</p>
                    </div>
                      <div class='col-sm-6 text-left p-2'>
                        <p >{$comment->date}</p>
                    </div>
             </div>
             <div class='row'>
                     <div class='col-sm-8 text-right p-4'>
                        <p>{$comment->comment}</p>
                     </div>
             </div> 
             <div class='row'>
                     <div class='col-sm-12 text-left'>
                        <button class='btn btn-light btn-sm mb-2' id='bt{$comment->id}'
                         @click.once='commentResponse({$comment->id})'>
                         پاسخ
                        </button>
                     </div>
             </div><hr>";
            foreach ($comment->responses as $response) {
                if ($response->status == 1) {
                    $commentBody .= " 
                     <div class='row'>
                        <div class='col-sm-2'> </div>
                        <div class='col-sm-9 p-2 mb-2'>
                        <div class='row bg-dark text-light' style='max-height: 35px'>
                                <div class='col-sm-6 text-right'>
                                    <p class='p-2'><i class='ml-2 fas fa-user'></i> {$response->user->name}</p>
                                </div>
                                <div class='col-sm-6 text-left p-2'>
                                    <p >{$response->date}</p>
                                </div>
                         </div>
                         <div class='row'>
                                <div class='col-sm-12 text-right  p-4'>
                                          {$response->comment}
                                </div>
                         </div>
                      </div>
                     </div>";
                }
            }
            $commentBody .= " 
              <div class='row' data-comment='{$comment->id}'> </div>";
        }
        return $commentBody;
    }

}