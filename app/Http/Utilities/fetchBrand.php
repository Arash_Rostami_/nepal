<?php
/**
 * Created by PhpStorm.
 * User: Arash Rostami
 * Date: 2/5/2020
 * Time: 3:54 PM
 */

namespace App\Http\Utilities;


class fetchBrand
{
    public function fetchBrand($products)
    {
        foreach ($products->brands as $brand) {

            return $brand->name;
        }

    }

}