<?php
/**
 * Created by PhpStorm.
 * User: Arash Rostami
 * Date: 3/27/2020
 * Time: 5:14 AM
 */

namespace App\Http\Utilities;


class countShopItem
{

    public function countEachItem ($lists, $item)
    {
        $differences= array_diff_key($lists->toArray(), $lists->unique('product_id')->toArray());
        $count =1;
        foreach($differences as $diff){
            if($diff['product_id'] == $item->product_id){
                $count++;
            }
        }
        return $count;

    }

}