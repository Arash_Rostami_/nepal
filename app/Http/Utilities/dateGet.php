<?php
/**
 * Created by PhpStorm.
 * User: Arash Rostami
 * Date: 1/17/2020
 * Time: 2:31 AM
 */

namespace App\Http\Utilities;


use Morilog\Jalali\Jalalian;

class dateGet
{
    public function getDate()
    {
        $date = Jalalian::forge('today')->format('%A, %d %B %y');
        return $date;

    }

}