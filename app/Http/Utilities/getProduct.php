<?php
/**
 * Created by PhpStorm.
 * User: Arash Rostami
 * Date: 2/25/2020
 * Time: 1:18 AM
 */

namespace App\Http\Utilities;


use App\Product;
use App\Size;
use Morilog\Jalali\Jalalian;

class getProduct
{
    public function getProName($id)
    {
        return Product::find($id)->name;
    }

    public function getProSize($id)
    {
        return Size::find($id)->size;
    }
    public function getProPrice($id)
    {
        $product = Product::find($id);
        $discounts= $product->discounts;
        $price = $product->price;

        if(count($discounts)>0) {
            foreach ($discounts as $discount){
                $pro_discount=$discount->value;
                $discount_date=$discount->enddate;
            }
            if($discount_date>Jalalian::now()){
                $dis= ($price * $pro_discount)/100;
                $price= $price-$dis;
            }
        }

        return $price;
    }
    public function getProImage($id)
    {
        foreach(Product::find($id)->images as $image){
            return $image->imagepath;
        }
    }
    public function getProAttr($id)
    {
        foreach(Product::find($id)->attributes as $attribute){
            foreach ($attribute->attributeItems as $attributeItem){
                echo $attribute->name . '=' .$attributeItem->name . PHP_EOL;
            }

        }
    }
}