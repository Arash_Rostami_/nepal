<?php
/**
 * Created by PhpStorm.
 * User: Arash Rostami
 * Date: 2/1/2020
 * Time: 7:10 PM
 */

namespace App\Http\Utilities;


class writeDiscount
{

    public function writeDisc($product)
    {
        if ($product->discounts->isEmpty()) {
            echo "<h4>";
            echo number_format($product->price);
            echo " تومان</h4>";
        } else {
            foreach ($product->discounts as $discount) {
                echo "<h6 class='d-inline text-secondary' style='text-decoration: line-through'>";
                echo number_format($product->price);
                echo " تومان  </h6>";
                echo "<br><h4 class='d-inline' >";
                echo discountCal($product->price, $discount->value);
                echo " تومان  </h4>  ";
            }
        }


    }


}