<?php
/**
 * Created by PhpStorm.
 * User: Arash Rostami
 * Date: 2/17/2020
 * Time: 4:04 PM
 */

namespace App\Http\Utilities;


class similarPros
{
    public function others($groups, $product, $discounts)
    {
        foreach($groups as $allgroup){
            if($allgroup->id == $product->group_id){
                foreach($groups as $group){
                    if($group->category_id ==$allgroup->category_id){
                        foreach($group->products as $pro){
                            if($pro->id != $product->id){
                                showTitle();
                                echo " <div id=\"pro\" class=\"d-inline-block \">";
                                checkDiscount($discounts, $pro);
                                checkProduct($pro);
                                echo "<h5 class=\"text-center bg-light p-3 \"> {$pro->name}</h5>
                                <p class=\"pro-desc\" title=\"{$pro->description}\"> 
                                {$pro->description}</p>";
                                calDiscount($discounts, $pro);
                                echo '</div>';
                            }
                        }
                    }
                }
            }
        }
    }
}