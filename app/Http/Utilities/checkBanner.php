<?php
/**
 * Created by PhpStorm.
 * User: Arash Rostami
 * Date: 1/13/2020
 * Time: 1:03 AM
 */

namespace App\Http\Utilities;


use App\Banner;

class checkBanner
{
    public function checkBan($parent)
    {
        $uniqueCode = array();
        $banners = Banner::all();
        foreach ($banners as $index => $banner) {
            array_push($uniqueCode, $banner->parent_id);
            if ($banner->parent_id == $parent->id && count(array_keys($uniqueCode, $banner->parent_id)) == 1) {
                echo "
                  <div class='card'>
                    <img class='card-img-top mx-auto' src='{$banner->image}'
                         alt='Card image'>
                    <div class='card-body text-center' style='background-color: #e3e3e3'>
                        {$banner->text}
                    </div>
                </div> ";
            }
        }

    }

}