<?php
/**
 * Created by PhpStorm.
 * User: Arash Rostami
 * Date: 1/13/2020
 * Time: 2:13 PM
 */

namespace App\Http\Utilities;


class calDiscount
{

    public function calDisc($discounts, $product)
    {

        if ($product->discounts->isEmpty()) {
            echo "<h5 class='text-center left price'>";
            echo number_format($product->price) ;
            echo "</h5>";
            echo "<h6> تومان</h6>";
        } else {
            foreach ($product->discounts as $discount) {
                echo "<h6 class='text-center left' style='text-decoration: line-through'>";
                echo number_format($product->price) ;
                echo " تومان  </h6>";
                echo "<h4 class='text-center text-danger left' >";
                echo discountCal($product->price, $discount->value) ;
                echo " تومان  </h4>  ";}
            }
        }

}