<?php
/**
 * Created by PhpStorm.
 * User: Arash Rostami
 * Date: 1/29/2020
 * Time: 11:37 PM
 */

namespace App\Http\Utilities;


class checkDiscountModal
{
    public function checkDisc($discounts, $product)
    {
        foreach($discounts as $discount){
            foreach($discount->products as $discount_pivot){
                if($discount_pivot->id == $product->id){
                    echo" 
            <div class=\"discount-slide\">
            <span class='discount-modal-shape'>
                  <span class='discount-modal-num'>{$discount->value}%</span>
            </span>
            </div>";
                }
            }
        }
    }
}