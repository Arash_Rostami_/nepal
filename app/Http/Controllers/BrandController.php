<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Group;
use App\ParentCategory;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;


class BrandController extends Controller
{

    public function index()
    {
        $brands = Brand::orderby('id', 'desc')->simplePaginate(5);
        return view('admin.brandshow', compact('brands'));
    }

    public function create()
    {
        $countries = countriesName();
        $parents = ParentCategory::all();
        return view('admin.brand', compact(['countries', 'parents']));
    }

    public function store(Request $request)
    {
        if ($request->brandname) {
            $this->saveData($request);
        }

        if ($request->file('file')) {
            $path = $this->savedImagepath($request);
            (Brand::latest('id', 'desc')->first())->update(['imagepath' => $path]);
        }
    }

    public function destroy($id)
    {
        $brand = Brand::find($id);
        $this->deletePath($brand);
        $brand->delete();
        flashMessage('با موفقیت', "  حذف گردید  ", 'success');
    }

    public function saveData(Request $request): void
    {
        $brand = new Brand([
            'name' => $request->brandname,
            'country' => $request->brandcountry,
            'parent_id' => $request->parentname,
        ]);
        $brand->save();
    }

    public function savedImagepath(Request $request): string
    {
        $file = $request->file('file');
        $fileName = uniqid() . $file->getClientOriginalName();
        $file->move(public_path('images/brands'), $fileName);
        $path = "/images/brands/{$fileName}";
        return $path;
    }

    public function deletePath($brand): void
    {
        $file_path = $brand->imagepath;
        if ($file_path) {
            unlink(public_path($file_path));
        }
    }

}
