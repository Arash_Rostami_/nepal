<?php

namespace App\Http\Controllers;

use App\Address;
use App\Attribute;
use App\Attributeitem;
use App\Banner;
use App\Brand;
use App\Category;
use App\City;
use App\Comment;
use App\Company;
use App\Discount;
use App\Group;
use App\Image;
use App\Notifications\UserQuestion;
use App\Order;
use App\ParentCategory;
use App\Product;
use App\Response;
use App\ShoppingList;
use App\Size;
use App\Slide;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Morilog\Jalali\Jalalian;

class HomePageController extends Controller
{

    public function __construct()
    {
//      $this->user = Auth::id();
    }

    public function search(Request $request)
    {


        $products = Product::where('name', 'LIKE', '%' . $request->data . '%')->get();
        $result = '';
        if (count($products) > 0) {
            foreach ($products as $product) {
                $result .= '<div class="text-right"><a class="text-decoration-none" href="/product/' . $product->id . '">
                <h6 class="search-name text-right text-secondary">' . $product->name . '</h6>
                </a></div><br>';
            }
            return $result;
        } else {
            return '<h6 class="search-name text-right text-danger">نتیجه ای یافت نشد....</h6>';
        }

    }


    public function error()
    {
        return view('main.test');
    }

    public function show()
    {

        list($addresses, $attrs, $attrItems, $banners, $brands, $categories, $cities, $comments,
            $companies, $discounts, $groups, $images, $orders, $parents, $products, $responses,
            $sizes, $slides, $users) = $this->fetchAllTables();


        return view('main.index', compact(['addresses', 'attrs', 'attrItems', 'banners', 'brands',
            'categories', 'cities', 'comments', 'companies', 'discounts', 'groups', 'images', 'orders', 'parents', 'products',
            'responses', 'sizes', 'slides', 'users']));

    }

    public function filter($id, Request $request)
    {
        $filters = Category::find($id);
        $sortBy = implode(array_keys($request->all()));
        $sortType = implode(array_values($request->all()));

        if (!empty($request->all())) {
            ($sortType == 'Asc')
                ?
                $products = $filters->products->sortBy($sortBy)->paginate(9)
                :
                $products = $filters->products->sortByDesc($sortBy)->paginate(9);
        } else {
            $products = $filters->products->paginate(9);
        }


        $addresses = Address::all();
        $attrs = Attribute::all();
        $attrItems = Attributeitem::all();
        $banners = Banner::all();
        $brands = Brand::all();
        $categories = Category::all();
        $cities = City::all();
        $comments = Comment::all();
        $companies = Company::all();
        $discounts = Discount::all();
        $groups = Group::all();
        $images = Image::all();
        $orders = Order::all();
        $parents = ParentCategory::all();
        $responses = Response::all();
        $sizes = Size::all();
        $slides = Slide::all();
        $users = User::all();

        return view('main.filter', compact(['addresses', 'attrs', 'attrItems', 'banners', 'brands',
            'categories', 'cities', 'comments', 'companies', 'discounts', 'groups', 'images', 'orders', 'parents', 'products',
            'responses', 'sizes', 'slides', 'users', 'filters']));

    }

    public function product($id)
    {
        $product = Product::find($id);

        $addresses = Address::all();
        $attrs = Attribute::all();
        $attrItems = Attributeitem::all();
        $banners = Banner::all();
        $brands = Brand::all();
        $categories = Category::all();
        $cities = City::all();
        $comments = Comment::all();
        $companies = Company::all();
        $discounts = Discount::all();
        $groups = Group::all();
        $images = Image::all();
        $orders = Order::all();
        $parents = ParentCategory::all();
        $responses = Response::all();
        $sizes = Size::all();
        $slides = Slide::all();
        $users = User::all();
        return view('main.product', compact(['addresses', 'attrs', 'attrItems', 'banners', 'brands',
            'categories', 'cities', 'comments', 'companies', 'discounts', 'groups', 'images', 'orders', 'parents', 'product',
            'responses', 'sizes', 'slides', 'users']));
    }

    public function checklogin()
    {
        list($addresses, $attrs, $attrItems, $banners, $brands, $categories, $cities, $comments,
            $companies, $discounts, $groups, $images, $orders, $parents, $products, $responses,
            $sizes, $slides, $users) = $this->fetchAllTables();
        $attributes = $attrs;
        $attributeitems = $attrItems;


        return view('main.check', compact(['addresses', 'attributes', 'attributeitems', 'banners', 'brands',
            'categories', 'cities', 'comments', 'companies', 'discounts', 'groups', 'images', 'orders', 'parents', 'products',
            'responses', 'sizes', 'slides', 'users']));


    }

    public function email(Request $request)
    {
        \Notification::route('mail', 'arashrostami@time-gr.com')->notify(new UserQuestion($request->phone));
        flashMessage('با موفقیت', 'پیامتان ارسال گردید', 'success');
        return back();
    }


    public function add(Request $request)
    {
        if ($request->edit == true) {
            $address = Address::where('id', $request->id)
                ->update([
                    'name' => $request->name,
                    'mobile' => $request->mobile,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'zipcode' => $request->zipcode,
                    'province' => $request->state,
                    'city' => $request->city,
                    'status' => 0,
                    'user_id' => $request->user,
                ]);
            return back();

        } else {
            $address = new Address([
                'name' => $request->name,
                'mobile' => $request->mobile,
                'phone' => $request->phone,
                'address' => $request->address,
                'zipcode' => $request->zipcode,
                'province' => $request->state,
                'city' => $request->city,
                'status' => 0,
                'user_id' => $request->user,
            ]);
            $address->save();
            return back();
        }

    }

    public function delete(Request $request)
    {

        $address = Address::destroy($request->id);
        return $address;

    }

    public function index($id)
    {
        return Address::find($id);
    }

    public function status(Request $request)
    {

        $id = $request->id;
        $select = Address::find($id);
        $user = $select->user_id;
        $select->status = 1;
        $select->save();
        Address::where('user_id', $user)->where('id', '!=', $id)->update([
            'status' => 0
        ]);
        return back();
    }

    public function fetchAllTables()
    {
        $addresses = Address::all();
        $attrs = Attribute::all();
        $attrItems = Attributeitem::all();
        $banners = Banner::all();
        $brands = Brand::all();
        $categories = Category::all();
        $cities = City::all();
        $comments = Comment::all();
        $companies = Company::all();
        $discounts = Discount::all();
        $groups = Group::all();
        $images = Image::all();
        $orders = Order::all();
        $parents = ParentCategory::all();
        $products = Product::all();
        $responses = Response::all();
        $sizes = Size::all();
        $slides = Slide::all();
        $users = User::all();

        return array($addresses, $attrs, $attrItems, $banners, $brands, $categories, $cities, $comments,
            $companies, $discounts, $groups, $images, $orders, $parents, $products, $responses, $sizes, $slides, $users);
    }

}
