<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Morilog\Jalali\Jalalian;


class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        if (isset($request->comment) && isset($request->title) && isset($request->size)) {
            $comment = new Comment([
                'title' => $request->title,
                'comment' => $request->comment,
                'size' => $request->size,
                'date' => Jalalian::forge('today')->format('%A, %d %B %y'),
                'group_id' => $request->group,
                'product_id' => $request->product,
                'user_id' => $request->user,
                'suggestion' => $request->suggestion,
                'status' => 0,
            ]);
            if ($comment->save()) {
                request()->session()->flash('key', 'Task was successful!');
                return back();
            };
        } else {
            request()->session()->flash('error', 'Task was unsuccessful!');
            return back();
        }
    }

    public function reply(Request $request)
    {
        if (isset($request->comment)) {
            $res = new Response([
                'title' => 'با کامنت شماره ' . $request->comment_id,
                'comment' => $request->comment,
                'comment_id' => $request->comment_id,
                'date' => Jalalian::forge('today')->format('%A, %d %B %y'),
                'user_id' => session('user'),
                'status' => 0,
            ]);
            if ($res->save()) {
                request()->session()->flash('key', 'Task was successful!');
                return back();
            }
        } else {
            request()->session()->flash('error', 'Task was unsuccessful!');
            return back();
        }
    }

    public function show(Request $request)
    {
        $comments = Comment::orderby('id', 'desc')->paginate(5, ['*'], 'comments');
        $responses = Response::orderby('id', 'desc')->paginate(5, ['*'], 'responses');

        return view('admin.commentshow', ['comments' => $comments, 'responses' => $responses]);
    }

    public function status(Request $request)
    {
        $comment = Comment::where('id', $request->id)->update([
            'status' => ($request->status == 0) ? '1' : '0'
        ]);
        return $comment;
    }

    public function destroy(Request $request)
    {
        Comment::where('id', $request->id)->delete();
    }

    public function state(Request $request)
    {
        $response = Response::where('id', $request->id)->update([
            'status' => ($request->status == 0) ? '1' : '0'
        ]);
        return $response;
    }

    public function delete(Request $request)
    {
        Response::where('id', $request->id)->delete();
    }
}
