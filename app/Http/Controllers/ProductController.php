<?php

namespace App\Http\Controllers;

use App\Attributeitem;
use App\Banner;
use App\Brand;
use App\Category;
use App\Discount;
use App\Group;
use App\Attribute;
use App\ParentCategory;
use App\Product;
use App\Size;
use Illuminate\Http\Request;
use App\Slide;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    public function index()
    {
        $product = $this->getLastProduct();
        $category = Category::all();
        $group = Group::all();
        $attribute = Attribute::all();
        $attributem = Attributeitem::all();
        $size = Size::all();
        $brand = Brand::all();
        $discount = Discount::all();

        return view('admin.product')
            ->with('product', $product)
            ->with('category', $category)
            ->with('attribute', $attribute)
            ->with('group', $group)
            ->with('attributem', $attributem)
            ->with('size', $size)
            ->with('brand', $brand)
            ->with('discount', $discount);

    }

    public function delete($id)
    {
        Group::find($id)->delete();
    }

    public function store(Request $request)
    {
        if ($request->name) {
            $product = new Product([
                'name' => $request->name,
                'description' => $request->desc,
                'price' => $request->price,
                'group_id' => $request->group,
            ]);
            $product->save();
        }
        if ($request->size) {
            $product = $this->getLastProduct();
            $product->sizes()->attach($request->size);
        }
        if ($request->attribute) {
            $product = $this->getLastProduct();
            $product->attributes()->attach($request->attribute);
        }
        if ($request->attr) {
            $product = $this->getLastProduct();
            $product->attributeitems()->attach($request->attr);
        }
        if ($request->brand) {
            $product = $this->getLastProduct();
            $product->brands()->attach($request->brand);
        }
        if ($request->discount) {
            $product = $this->getLastProduct();
            $this->insertIntoProductTable($request, $product);
            $product->discounts()->attach($request->discount);
        }
    }

    public function show()
    {
        $products = Product::all();
        $brands = Brand::all();
        $groups = Group::all();
        $sizes = Size::all();
        $attrs = Attribute::all();
        $attr_items = Attributeitem::all();
        $discounts = Discount::all();
        return view('admin.productshow', compact(['products', 'brands', 'groups', 'sizes', 'attrs', 'attr_items', 'discounts']));
    }

    public function update(Request $request)
    {
        $image = new ImageController();
        if ($request->file('file')) {
            $product = Product::find($request->id)->get();
            $path = $image->imageMove($request);
            $image->imageSave($path, $product);
        } else if ($request->id) {
            Product::where('id', $request->id)->update([
                'name' => $request->name,
                'description' => $request->desc,
                'price' => $request->price,
                'group_id' => $request->group,
            ]);
            return back();
        }
    }

    public function add(Request $request)
    {
        if ($request->prsize) {
            (Product::where('id', $request->prsize)->first())->sizes()->attach($request->size);
            flashMessage('با موفقیت', "  اضافه گردید  ", 'success');
            return back();

        } else if ($request->prattr) {
            (Product::where('id', $request->prattr)->first())->attributes()->attach($request->attr);
            flashMessage('با موفقیت', "  اضافه گردید  ", 'success');
            return back();

        } else if ($request->pritem) {
            (Product::where('id', $request->pritem)->first())->attributeitems()->attach($request->item);
            flashMessage('با موفقیت', "  اضافه گردید  ", 'success');
            return back();

        } else if ($request->prbrand) {
            (Product::where('id', $request->prbrand)->first())->brands()->attach($request->brand);
            flashMessage('با موفقیت', "  اضافه گردید  ", 'success');
            return back();

        } else if ($request->prdis) {
            $product = (Product::where('id', $request->prdis)->first());
            $product->discounts()->attach($request->discount);
            $product->update([
                'discount_id' => $request->discount,
            ]);
            flashMessage('با موفقیت', "  اضافه گردید  ", 'success');
            return back();

        }


    }

    public function del(Request $request)
    {
        if ($request->prsize) {
            Product::find($request->prsize)->sizes()->detach($request->size);
            flashMessage('با موفقیت', "  حذف گردید  ", 'success');
            return back();
        } else if ($request->prattr) {
            Product::find($request->prattr)->attributes()->detach($request->attr);
            flashMessage('با موفقیت', "  حذف گردید  ", 'success');
            return back();
        } else if ($request->pritem) {
            Product::find($request->pritem)->attributeitems()->detach($request->attr_item);
            flashMessage('با موفقیت', "  حذف گردید  ", 'success');
            return back();
        } else if ($request->prbrand) {
            Product::find($request->prbrand)->brands()->detach($request->brand);
            flashMessage('با موفقیت', "  حذف گردید  ", 'success');
            return back();
        } else if ($request->prdis) {
            $product = Product::find($request->prdis);
            $product->discounts()->detach($request->discount);
            $product->update([
                'discount_id' => NULL,
            ]);
            flashMessage('با موفقیت', "  حذف گردید  ", 'success');
            return back();
        }
    }

    public function destroy($id)
    {
        Product::find($id)->delete();
        flashMessage('با موفقیت', "  حذف گردید  ", 'success');
    }

    public function getLastProduct()
    {
        $product = (Product::latest()->first());
        return $product;
    }

    public function insertIntoProductTable(Request $request, $product): void
    {
        Product::find($product->id)->update(['discount_id' => $request->discount]);
    }

    public function uploadImage()
    {
        $brands = Brand::all();
        $parents = ParentCategory::all();
        $cats = Category::all();
        return view('admin/slide')
            ->with('brands', $brands)
            ->with('cats', $cats)
            ->with('parents', $parents);
    }

    public function uploadSlide(Request $request)
    {
        if ($request->file('file')) {
            $path = $this->slideNamed($request);
            $slide = new Slide([
                'image' => $path,
                'text' => $request->description,
                'parent_id' => $request->parent,
                'brand_id' => $request->brand,
                'name' => $request->name,
            ]);
            $slide->save();
        }


    }

    public function uploadBanner(Request $request)
    {
        if ($request->file('file')) {
            $path = $this->bannerNamed($request);
            $banner = new Banner([
                'image' => $path,
                'text' => $request->firstname,
                'parent_id' => $request->lastname,
            ]);
            $banner->save();
        }

    }

    public function slideNamed(Request $request): string
    {
        $file = $request->file('file');
        $fileName = uniqid() . $file->getClientOriginalName();
        $file->move(public_path('images/slides'), $fileName);
        $path = "/images/slides/{$fileName}";
        return $path;
    }

    public function bannerNamed(Request $request): string
    {
        $file = $request->file('file');
        $fileName = uniqid() . $file->getClientOriginalName();
        $file->move(public_path('images/banners'), $fileName);
        $path = "/images/banners/{$fileName}";
        return $path;
    }

}
