<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Order;
use App\Product;
use App\Response;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
        $comments = Comment::orderby('id', 'desc')->paginate(5, ['*'], 'comments');
        $responses = Response::orderby('id', 'desc')->paginate(5, ['*'], 'responses');
        $orders = Order::orderby('id', 'desc')->paginate(5, ['*'], 'orders');
        $user = User::all()->count();
        $pro = Product::all()->count();
        $comment = Comment::where('status', 1)->get()->count();
        $order = Order::all()->count();

        return view('admin.index', ['comments' => $comments, 'orders' => $orders, 'responses' => $responses,
            'user' => $user, 'pro' => $pro, 'comment' => $comment, 'order' => $order]);
    }
}
