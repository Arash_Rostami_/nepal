<?php

namespace App\Http\Controllers;

use App\Image;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ImageController extends Controller
{
    public function addImage(Request $request)
    {
        if ($request->file('file')) {
            $product = $this->productID();
            $path = $this->imageMove($request);
            $this->imageSave($path, $product);
        }
    }

    public function productID()
    {
        $product = (Product::latest('id', 'desc')->first())->id;
        return $product;
    }

    public function imageMove(Request $request): string
    {
        $file = $request->file('file');
        $fileName = uniqid() . $file->getClientOriginalName();
        $file->move(public_path('images'), $fileName);
        $path = "/images/{$fileName}";
        return $path;
    }

    public function imageSave($path, $product): void
    {
        $image = new Image([
            'imagepath' => $path,]);
        $image->save();
        $image->products()->attach($product);
    }

    public function destroy($id)
    {
        $file_path = Image::find($id)->imagepath;
        unlink(public_path($file_path));
        Image::find($id)->delete();

    }

}
