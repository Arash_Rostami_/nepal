<?php

namespace App\Http\Controllers;

use App\Address;
use App\Comment;
use App\Company;
use App\Notifications\UserQuery;
use App\Order;
use App\ShoppingList;
use App\User;
use Auth;
use App\Exceptions;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\HomePageController;

class UserController extends Controller
{

    public function empty(Request $request)
    {
        $del = ShoppingList::where('product_id', $request->pro)->where('user_id', $request->user)->delete();
        if ($del)
            flashMessage('با موفقیت', "  حذف گردید  ", 'success');
    }

    public function list($id)
    {
        $user = User::find($id);
        $date = Carbon::today()->subDays(7);
        $lists = ShoppingList::where('user_id', $id)->where('created_at', '>', $date)->get();
        $homePageController = new HomePageController();
        list($attrs, $attrItems, $brands, $parents, $categories, $groups, $slides, $banners, $latestban, $products, $discounts) = $homePageController->fetchAllTables();
        return view('partials.userlists', compact(['products', 'parents', 'groups', 'categories', 'brands', 'slides',
            'banners', 'latestban', 'discounts', 'attrs', 'attrItems', 'lists', 'user']));

    }

    public function submit(Request $request)
    {
        $user = User::find($request->user);
        $question = $request->question;
        $email = $user->email;
        $username = $user->name . ' ' . $user->lname;
        \Notification::route('mail', 'arashrostami@time-gr.com')->notify(new UserQuery($question, $username, $email));
        flashMessage('با موفقیت', 'سوالتان ارسال گردید', 'success');
        return back();

    }

    public function question(Request $request)
    {
        $user = User::find($request->id);
        $homePageController = new HomePageController();
        list($attrs, $attrItems, $brands, $parents, $categories, $groups, $slides, $banners, $latestban, $products, $discounts) = $homePageController->fetchAllTables();
        return view('partials.userquery', compact(['products', 'parents', 'groups', 'categories', 'brands', 'slides',
            'banners', 'latestban', 'discounts', 'attrs', 'attrItems', 'user']));
    }

    public function comment(Request $request)
    {
        $user = User::find($request->id);
        $comments = Comment::where('user_id', $request->id)->paginate(6);
        $homePageController = new HomePageController();
        list($attrs, $attrItems, $brands, $parents, $categories, $groups, $slides, $banners, $latestban, $products, $discounts) = $homePageController->fetchAllTables();
        return view('partials.usercomments', compact(['products', 'parents', 'groups', 'categories', 'brands', 'slides',
            'banners', 'latestban', 'discounts', 'attrs', 'attrItems', 'user', 'comments']));


    }

    public function order(Request $request)
    {

        $user = User::find($request->id);
        $orders = Order::where('user_id', $request->id)->orderby('id', 'desc')->simplePaginate(5);
        $homePageController = new HomePageController();
        list($attrs, $attrItems, $brands, $parents, $categories, $groups, $slides, $banners, $latestban, $products, $discounts) = $homePageController->fetchAllTables();
        return view('partials.userorders', compact(['products', 'parents', 'groups', 'categories', 'brands', 'slides',
            'banners', 'latestban', 'discounts', 'attrs', 'attrItems', 'user', 'orders']));

    }

    public function mainad(Request $request)
    {
        $id = $request->id;
        $select = Address::find($id);
        $user = $select->user_id;
        $select->status = 1;
        $select->save();
        Address::where('user_id', $user)->where('id', '!=', $id)->update([
            'status' => 0
        ]);
        return back();

    }

    public function ad($id)
    {

        $user = User::find($id);
        $addresses = Address::where('user_id', $id)->get();
        $homePageController = new HomePageController();
        list($attrs, $attrItems, $brands, $parents, $categories, $groups, $slides, $banners, $latestban, $products, $discounts) = $homePageController->fetchAllTables();
        return view('partials.useraddress', compact(['products', 'parents', 'groups', 'categories', 'brands', 'slides',
            'banners', 'latestban', 'discounts', 'attrs', 'attrItems', 'user', 'addresses']));
    }

    public function ind(Request $request)
    {
        $user = Auth::id();
        if (isset($request->gender)) {
            $birth = ($request->dbirth . '/' . $request->mbirth . '/' . $request->ybirth);
            $user = User::where('id', $user)->update([
                'name' => $request->fname,
                'lname' => $request->lname,
                'idcode' => $request->idcode,
                'gender' => $request->gender,
                'birthdate' => $birth,
                'accountno' => $request->accountno,
            ]);
            if (Address::where('user_id', $user)->exists()) {
                $address = Address::where('user_id', $user)->update([
                    'name' => $request->fname . $request->lname,
                    'mobile' => $request->mobile,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'zipcode' => $request->zipcode,
                    'province' => $request->state,
                    'city' => $request->city,
                ]);
            } else {
                $address = new Address([
                    'name' => $request->fname . $request->lname,
                    'mobile' => $request->mobile,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'zipcode' => $request->zipcode,
                    'province' => $request->state,
                    'city' => $request->city,
                    'user_id' => $user,
                    'status' => 0,
                ]);
                $address->save();
                flashMessage('با موفقیت', 'اطلاعات شخصی شما ذخیره گردید', 'success');
            }
        }
        if (isset($request->cname)) {
            if (Company::where('user_id', $user)->exists()) {
                $company = Company::where('user_id', $user)->update([
                    'name' => $request->cname,
                    'code' => $request->cbnumber,
                    'idcode' => $request->cidcode,
                    'phone' => $request->cphone,
                    'number' => $request->cregicode,
                    'province' => $request->cstate,
                    'city' => $request->ccity,
                    'user_id' => $user,
                    'address' => $request->caddress,
                ]);
                flashMessage('با موفقیت', 'اطلاعات شخصی شما بروز رسانی شد', 'success');
                return back();
            } else {
                $company = new Company([
                    'name' => $request->cname,
                    'code' => $request->cbnumber,
                    'idcode' => $request->cidcode,
                    'phone' => $request->cphone,
                    'number' => $request->cregicode,
                    'province' => $request->cstate,
                    'city' => $request->ccity,
                    'user_id' => $user,
                    'address' => $request->caddress,
                ]);
                $company->save();
                flashMessage('با موفقیت', 'اطلاعات شخصی شما ذخیره گردید', 'success');
                return back();
            }
        }
        flashMessage('خطا', 'اطلاعات شخصی شما ذخیره نگردید', 'error');
        return back();
    }

    public function profile($id)
    {
        $user = User::find($id);
        $homePageController = new HomePageController();
        list($attrs, $attrItems, $brands, $parents, $categories, $groups, $slides, $banners, $latestban, $products, $discounts) = $homePageController->fetchAllTables();
        return view('partials.profile', compact(['products', 'parents', 'groups', 'categories', 'brands', 'slides',
            'banners', 'latestban', 'discounts', 'attrs', 'attrItems', 'user']));
    }

    public function info($id)
    {
        if (Auth::check()) {
            $user = User::find($id);
            $companies = Company::where('user_id', $id)->get();
            $homePageController = new HomePageController();
            list($attrs, $attrItems, $brands, $parents, $categories, $groups, $slides, $banners, $latestban, $products, $discounts) = $homePageController->fetchAllTables();
            if (count($companies) > 0) {
                foreach ($companies as $company) ;
                return view('partials.userinfo', compact(['company', 'products', 'parents', 'groups', 'categories', 'brands', 'slides',
                    'banners', 'latestban', 'discounts', 'attrs', 'attrItems', 'user']));
            } else {
                return view('partials.userinfo', compact(['products', 'parents', 'groups', 'categories', 'brands', 'slides',
                    'banners', 'latestban', 'discounts', 'attrs', 'attrItems', 'user']));
            }

        }


    }

    public function index()
    {
        return view('admin.user');
    }

    public function store(Request $request)
    {
        if ($request->file('file')) {
            $file = $request->file('file');
            $fileName = $this->createImageName($request, $file);
            $this->saveImage($file, $fileName);
        }
        if ($request->email && $request->password) {
            $this->saveFile($request);
        }

    }

    public function show()
    {
        $users = User::orderby('id', 'desc')->get();
        return view('admin.usershow', compact('users'));
    }

    public function status(Request $request)
    {
        $id = $request->id;
        $status = User::find($id)->status;

        if ($status == 1) {
            $user = User::where('id', $id)->where('status', $status)->update(['status' => 0]);
        } else if ($status == 0) {
            $user = User::where('id', $id)->where('status', $status)->update(['status' => 1]);
        }
        return $user;
    }

    public function retrieve($id)
    {
        $user = User::where('id', $id)->first();
        return $user;
    }

    public function edit(Request $request)
    {
        return $this->editUser($request);
    }

    public function fetchstatus()
    {
        return User::all();
    }

    public function destroy($id)
    {
        User::find($id)->delete();
        return flashMessage('با موفقیت', "  حذف گردید  ", 'success');
    }

    public function createImageName(Request $request, $file): string
    {
        if (!empty($file)) {
            $prefix = $request->firstname . '-' . $request->lastname . '-';
            $fileName = $prefix . $file->getClientOriginalName();
            return $fileName;
        }
    }

    public function saveImage($file, string $fileName): void
    {
        $file->move(public_path('images/users/'), $fileName);
    }

    public function retrieveImageName(Request $request): string
    {
        $path = '/images/users/' . $request->fname . '-' . $request->lname . '-' . $request->image;
        return $path;

    }

    public function saveFile(Request $request): void
    {
        if (empty($request->image)) {
            $user = new User([
                'name' => $request->fname,
                'lname' => $request->lname,
                'role' => $request->role,
                'email' => $request->email,
                'password' => $request->password,
            ]);
            $user->save();
        }

        if (!empty($request->image)) {
            $user = new User([
                'name' => $request->fname,
                'lname' => $request->lname,
                'role' => $request->role,
                'email' => $request->email,
                'password' => $request->password,
                'image' => $this->retrieveImageName($request),
            ]);
            $user->save();
        }
    }

    public function editUser(Request $request)
    {
        if (empty($request->image)) {
            User::where('id', $request->id)->update([
                'name' => $request->fname,
                'lname' => $request->lname,
                'role' => $request->role,
                'email' => $request->email,
                'password' => $request->password
            ]);
            return flashMessage('با موفقیت', "  ثبت گردید  ", 'success');
        }

        if (!empty($request->image)) {
            User::where('id', $request->id)->update([
                'name' => $request->fname,
                'lname' => $request->lname,
                'role' => $request->role,
                'email' => $request->email,
                'password' => $request->password,
                'image' => $this->retrieveImageName($request),
            ]);
            return flashMessage('با موفقیت', "  ثبت گردید  ", 'success');
        }
    }
}
