<?php

namespace App\Http\Controllers;

use App\Discount;
use App\Jobs\SendMail;
use App\Mail\OrderStatus;
use App\Order;
use App\Product;
use App\ShoppingList;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Morilog\Jalali\Jalalian;
use Zarinpal\Zarinpal;

class CardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public $qty = 1;
    public $totalPrice = 0;

    public function add($id, Request $request)
    {

            $product = Product::find($id);
            ShoppingList::create([
                'product_id' => $request->product_id,
                'size_id' => $request->size_id,
                'user_id' => Auth::id(),
            ]);

            if (Session::has('shopCart')) {
                list($cart, $pro) = $this->updateSessions($product);
            } else {
                $cart = ['qty' => $this->qty, 'price' => getProductPrice($product->id),
                    'total' => $this->totalPrice + getProductPrice($product->id)];
                $pro = [$product->id];
            }
            session(['shopCart' => $cart]);
            session(['pro' => $pro]);

    }


    public function final($id)
    {
    $date = Carbon::today()->subDays(7);
        $lists = ShoppingList::where('user_id', $id)->where('created_at', '>', $date)->get();
        $price = 0;
        $qty = 0;
        $pro = array();
        $sizeO = array();
        $test = array();

        foreach ($lists as $list) {
            $price += getProductPrice($list->product_id);
            $qty++;

            array_push($pro, $list->product_id);
            $sizeO += array('product' . $list->product_id => $list->size_id);


            Session::forget('pro');
            Session::forget('shopCart');


            $cart = ['qty' => $qty, 'price' => $price, 'total' => $price];
            session(['shopCart' => $cart]);
            session(['pro' => $pro]);
        }


        $home = new HomePageController();
        list($addresses, $attrs, $attrItems, $banners, $brands, $categories, $cities, $comments,
            $companies, $discounts, $groups, $images, $orders, $parents, $products, $responses,
            $sizes, $slides, $users) = $home->fetchAllTables();
        $attributes = $attrs;
        $attributeitems = $attrItems;


        return view('main.checkout', compact(['addresses', 'attributes', 'attributeitems', 'banners', 'brands',
            'categories', 'cities', 'comments', 'companies', 'discounts', 'groups', 'images', 'orders', 'parents', 'products',
            'responses', 'sizes', 'slides', 'users', 'sizeO']));


    }


    public function remove(Request $request, $id)
    {
        $product = Product::find($request->value);
        $item = ShoppingList::where('product_id', $request->value)->first();
        if ($item->delete()) {

            Session::forget('pro.' . $id);

            $newCart = $this->updateShopCart($product);

            Session::forget('shopCart');

            session(['shopCart' => $newCart]);

        }

    }


    public function destroy(Request $request, $id)
    {
        list($cart, $product, $num, $item_removed, $sum) = $this->getDataOfSessions($request);
        ShoppingList::where('product_id', $product->id)->where('user_id', Auth::id())->delete();
        $this->deleteSessions();
        $this->setSessionShopCart($cart, $num, $product, $sum);
        $this->setSessionPro($item_removed);

    }


    public function updateShopCart($product): array
    {
        $cart = Session::get('shopCart');
        $newCart = ['qty' => $cart['qty'] - 1, 'price' => getProductPrice($product->id),
            'total' => $cart['total'] - getProductPrice($product->id)];
        return $newCart;
    }


    public function updateSessions($product): array
    {
        $cart = Session::get('shopCart');
        $pro = Session::get('pro');
        $cart['qty']++;
        $cart['total'] += getProductPrice($product->id);
        array_push($pro, $product->id);

        return array($cart, $pro);
    }


    public function getDataOfSessions(Request $request): array
    {
        $pro = Session::get('pro');
        $cart = Session::get('shopCart');
        $product = Product::find($request->value);

        $num = count(array_keys($pro, $request->value));
        $item_removed = array_diff($pro, ["$request->value"]);
        $sum = getProductPrice($product->id) * $num;
        return array($cart, $product, $num, $item_removed, $sum);
    }

    public function deleteSessions(): void
    {
        Session::forget('pro');
        Session::forget('shopCart');
    }

    public function setSessionShopCart($cart, $num, $product, $sum): void
    {
        $newCart = ['qty' => $cart['qty'] - $num, 'price' => $product->price,
            'total' => $cart['total'] - $sum];
        session(['shopCart' => $newCart]);
    }

    public function setSessionPro($item_removed): void
    {
        session(['pro' => $item_removed]);
    }

    public function zarinpal(Request $request)
    {
        list($id_orders, $sum, $date, $address_id, $type, $factor) = $this->getData($request);

        $this->saveOrder($date, $sum, $id_orders, $address_id, $type, $factor);

        if ($type == 'online') {
            //  $this->zarinpalGate($sum, $id_orders);
        }

    }

    public function verify()
    {
        if ((Session::get('type') == 'online')) {
            // $results = $this->getZarinpalData();

            $order = Order::find(Session::get('order_id'))->update([
                'status' => 1,
                //    'id_trans' => $results['RefID']
            ]);
        }
        //    return json_encode(Session::get('id_orders'));
        return view('main.transaction');


    }

    public function getData(Request $request): array
    {
        $type = ($request->type == 'real') ? 'real' : 'online';
        $address_id = $request->id;
        $factor = $request->mail;
        $id_orders = time() . uniqid() . 'np';
        $sum = Session::get('shopCart.total');
        $date = Jalalian::forge('today')->format('%A, %d %B %y');
        return array($id_orders, $sum, $date, $address_id, $type, $factor);
    }

    public function saveOrder($date, $sum, $id_orders, $address_id, $type, $factor): void
    {
        $date = Jalalian::forge('today')->format('%A, %d %B %y');
        $order = new Order([
            'date' => $date,
            'price' => $sum,
            'status' => 0,
            'action' => 0,
            'type' => $type,
            'id_trans' => null,
            'id_orders' => $id_orders,
            'user_id' => Auth::user()->id,
            'address_id' => $address_id,
        ]);
        $order->save();
        foreach (Session::get('pro') as $product_id) {

            $pro = Product::find($product_id);
            $pro->orders()->attach($order);
            ShoppingList::where('product_id', $pro->id)->where('user_id', Auth::id())->delete();

        }
        if($factor=='yes'){
            $this->dispatch(new SendMail(Auth::user(), $order, $date));
        }
        $this->deleteSessions();
        Session::put([
            'order_id' => $order->id,
            'id_orders' => $id_orders,
            'type' => $type
        ]);
    }

    public function zarinpalGate($sum, $id_orders): void
    {
        $zarinpal = new Zarinpal('XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX');
        $zarinpal->enableSandbox();
        $results = $zarinpal->request(
            "/verify",                              //required
            $sum,                                              //required
            'شماره فاکنور-' . $id_orders,           //required
            Auth::user()->email,                              //optional
            '09000000000');                             //optional

        echo json_encode($results);

        if (isset($results['Authority'])) {
            file_put_contents('Results', $results);
            $zarinpal->redirect();
        }
    }

    public function getZarinpalData()
    {
        $zarinpal = new Zarinpal('XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX');
        $results = file_get_contents('Results');
        $authority = $results['Authority'];
        echo json_encode($zarinpal->verify('OK', 1000, $authority));
        return $results;
    }


}
