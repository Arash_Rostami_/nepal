<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function show()
    {
        $orders = Order::orderby('id', 'desc')->paginate(5, ['*'], 'orders');
        return view('admin.ordershow', compact('orders'));
    }

    public function update(Request $request)
    {
        $order = Order::find($request->id);
        $order->action == 0 ? $order->action = 1 : $order->action = 0;
        $order->save();
    }

    public function report()
    {
        $order = Order::select(DB::raw("COUNT(type) AS total, date"))
            ->groupBy('date')->orderBy('date', 'desc')->get();
        return $order;
    }
}
