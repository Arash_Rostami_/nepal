<?php

namespace App\Http\Controllers;

use App\Discount;
use App\Product;
use Illuminate\Http\Request;

class DiscountController extends Controller
{
    public function index()
    {
        return view('admin.discount');
    }

    public function store(Request $request)
    {
        $product = $this->findProduct($request);
        $discount = $this->saveDiscount($request);
        $this->linkToProduct($product, $discount);
    }

    public function show()
    {
        $discounts = Discount::orderby('id', 'desc')->simplePaginate(5);
        return view('admin.discountshow', compact('discounts'));
    }

    public function products()
    {
        return Product::all();
    }

    public function destroy($id)
    {
        $discount = Discount::find($id);
        $discount->delete();
        flashMessage('با موفقیت', "  حذف گردید  ", 'success');
    }

    public function findProduct(Request $request)
    {
        $product = Product::where('id', $request->discountProduct)->first();
        return $product;
    }

    public function linkToProduct($product, Discount $discount): void
    {
        $product->discounts()->attach($discount->id);
    }

    public function saveDiscount(Request $request): Discount
    {
        $discount = new Discount([
            'name' => $request->discountName,
            'value' => $request->discountValue,
            'code' => $request->discountCode,
            'begindate' => $request->discountBeginDate,
            'enddate' => $request->discountEndDate,
        ]);
        $discount->save();
        return $discount;
    }
}
