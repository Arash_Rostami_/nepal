<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $table = 'sizes';
    protected $fillable = [
        'size', 'group_id'
    ];

    public function group()
    {
        return $this->hasOne(Group::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function shoppinglists()
    {
        return $this->hasMany(ShoppingList::class);
    }

    public function getGroup($id)
    {
        $groups = Group::find($id)->get();
            foreach($groups as $group){
                return $group->name;
            }


    }
}
