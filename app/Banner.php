<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banners';
    public $timestamps = false;
    protected $fillable = [
        'image', 'parent_id', 'text'
    ];
}
