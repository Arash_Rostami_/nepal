<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';
    protected $fillable = [
        'date' ,'price', 'status', 'type', 'id_trans', 'id_orders', 'user_id', 'product_id', 'address_id'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_product');
    }

    public function address()
    {
        return $this->belongsTo(Address::class, 'address_id');
    }


}
