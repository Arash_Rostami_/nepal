<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Order;
Use App\User;
Use Morilog\Jalali\Jalalian as Date;

class OrderStatus extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $order;
    public $date;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Order $order, $date)
    {
        $this->user= $user;
        $this->order= $order;
        $this->date= $date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(  'فاکتور تراکنش برای ' .$this->user->name)
                     ->view('mails.order');
    }
}
