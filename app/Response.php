<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $table = 'responses';
    public $timestamps = false;
    protected $fillable = [
        'title', 'comment', 'date',
        'status', 'comment_id', 'user_id',
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comment()
    {
        return $this->belongsTo(Comment::class, 'comment_id');
    }
}
