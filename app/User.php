<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as
    CanResetPasswordContract;

class User extends Authenticatable implements  CanResetPasswordContract
{
    use Notifiable;
    use AuthenticatableContract;

    protected $fillable = [
        'name', 'email', 'password', 'lname', 'idcode', 'phone',
        'gender', 'birthdate', 'accountno', 'city_id', 'role', 'image', 'status' , 'google_id'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
//    protected $hidden = [
//        'password', 'remember_token',
//    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function companies()
    {
        return $this->hasMany(Company::class);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function responses()
    {
        return $this->hasMany(Response::class);
    }

    public function shoppinglists()
    {
        return $this->hasMany(ShoppingList::class);

    }

}
