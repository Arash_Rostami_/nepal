<?php

namespace App;

use App\Http\Controllers\GroupController;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    public $timestamps = false;
    protected $fillable = [
        'title', 'comment', 'date', 'size', 'group_id',
        'product_id', 'status', 'suggestion', 'user_id',
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function Group()
    {
        return $this->belongsTo(Group::class);
    }

    public function responses()
    {
        return $this->hasMany(Response::class);
    }
    public function sizes()
    {
        return $this->belongsTo(Size::class, 'size');
    }

}
