<?php

namespace App\Jobs;

use App\Mail\OrderStatus;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class SendMail
{
    use Dispatchable, Queueable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $order, $date)
    {
        $this->user=$user;
        $this->order=$order;
        $this->date=$date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Mail::to('arashrostami@time-gr.com')->send(new OrderStatus($this->user, $this->order, $this->date));
    }
}
