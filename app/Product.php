<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $table = 'products';
    protected $fillable = [
        'name', 'description', 'price', 'attribute_id', 'group_id',
        'discount_id', 'size_id', 'brand_id', 'attributeitem_id',
    ];


    public function attributes()
    {
        return $this->belongsToMany(Attribute::class);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_product', 'product_id','order_id');
    }

    public function brands()
    {
        return $this->belongsToMany(Brand::class);
    }

    public function images()
    {
        return $this->belongsToMany(Image::class);
    }

    public function attributeitems()
    {
        return $this->belongsToMany(Attributeitem::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function shoppinglists()
    {
        return $this->hasMany(ShoppingList::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function discounts()
    {
        return $this->belongsToMany(Discount::class);
    }

    public function sizes()
    {
        return $this->belongsToMany(Size::class);
    }

    public function getDiscounts($id)
    {
        $discounts = $this->find($id)->discounts;
        foreach ($discounts as $discount) {
            return ($discount->name) . " (" . $discount->value . "%)";
        }

    }

    public function getImages($id)
    {
        $images = $this->find($id)->images;
        for ($i = 0; $i < count($images); $i++) {
            echo "<div class='col-sm-6 rounded '>
             <img src='" . ($images[$i]->imagepath) . "' width='60%'>
             </div><br>";
        }
    }

    public function getImagesEdit($id)
    {
        $images = $this->find($id)->images;
        for ($i = 0; $i < count($images); $i++) {
            echo "<div class='col-md-4 rounded '>
             <img src='" . ($images[$i]->imagepath) . "' width='60%'>
              <span class='close' @click='deleteimg({$images[$i]->id})'>x</span>
             </div>";
        }
    }

    public function getBrandsName($id)
    {
        $brands = $this->find($id)->brands;
        foreach ($brands as $brand) {
            return $brand->name;
        }

    }

    public function getBrandsCountry($id)
    {
        $brands = $this->find($id)->brands;
        foreach ($brands as $brand) {
            return $brand->country;
        }

    }

    public function getBrandsPhoto($id)
    {
        $brands = $this->find($id)->brands;
        foreach ($brands as $brand) {
            return $brand->imagepath;
        }

    }

    public function getSize($id)
    {
        $sizes = $this->find($id)->sizes;
        foreach ($sizes as $size) {
            return $size->size;
        }

    }



}
