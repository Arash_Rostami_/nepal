<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('google_id')->nullable();
            $table->string('name');
            $table->string('lname')->nullable();
            $table->smallInteger('role');
            $table->integer('status')->nullable();
            $table->text('image')->nullable();
            $table->string('idcode')->nullable();
            $table->smallInteger('gender')->nullable();
            $table->string('birthdate')->nullable();
            $table->string('accountno')->nullable();
            $table->bigInteger('city_id')->unsigned()->nullable();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
