<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributeitemProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributeitem_product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('attributeitem_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->foreign('attributeitem_id')->references('id')->on('attributeitems')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('product_id')->references('id')->on('products')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributeitem_product');
    }
}
